import validation from "../validation.js";

const show_loading = function (form) {
    $(form).find('[type=submit]').attr('disabled', 'disabled');
    $(form).find('[type=submit]').after(
        `<div class="text-center js-loading mt-3">
            <div class="spinner-border"> </div>
            Processing! Please wait...
        </div>`
    );
}

const hide_loading = function (form) {
    $(form).find('[type=submit]').removeAttr('disabled');
    $(form).find('.js-loading').remove();
}

const form_reset = function (form) {
    $(form).find('.js-message').remove();
}

const show_success_msg = function (form, msg) {
    $(form).prepend(
        `<div class="alert alert-success js-message">
            ${msg}
        </div>`
    );
}

const show_error_msg = function (form, msg) {
    $(form).prepend(
        `<div class="alert alert-danger js-message">
            ${msg}
        </div>`
    );
}

$(function () {
    $('form').attr('novalidate', true);

    $(document).on('submit', 'form:not(.ajax-form)', function (e) {
        if (!validation(this)) e.preventDefault();
    });

    $(document).on('submit', '#agentLoginForm', function (e) {
        e.preventDefault();
        let self = this;
        let is_success = validation(self);

        form_reset(self);

        if (is_success) {
            show_loading(self);
            $.ajax({
                url: $(self).attr('action'),
                type: 'POST',
                data: $(self).serialize(),
                success: function (result) {
                    // show_success_msg(self, result.message);
                    $('#divSuccessMsg').html('<div class="alert alert-success js-message">'+ result.message +'</div>').hide()
                    
                    .fadeIn(1500, function() { $('#divSuccessMsg'); });
                   
                    hide_loading(self);

                    if (result.auth) {
                        setTimeout(function () {
                            location.reload();
                        }, 10);
                    } else {
                        $(self).find('[type=submit]').html('Login');

                        $(self).find('.form-group').last().after(`
                            <div
                                class="form-group"
                            >
                                <label for="password">Password *</label>
                                <input 
                                    type="password"
                                    minlength="8"
                                    maxlength="20"
                                    class="form-control"
                                    placeholder="Enter Your Password"
                                    name="password"
                                    id="password"
                                    required
                                >
                            </div>
                            <div
                                class="form-group new-elements"
                            >
                                <label for="otp_code">OTP Code</label>
                                <input 
                                    type="tel" 
                                    minlength="6" 
                                    maxlength="6" 
                                    name="otp_code" 
                                    class="form-control"
                                    placeholder="Enter OTP Code"
                                    id="otp_code" 
                                    required
                                >
                            </div>
                            <div style="padding: 5px 0;">
                                <a class="resendOtp" style="color:#0000FF; cursor:pointer;margin-bottom:10px;">Resend Otp</a>
                            </div>
                        `);
                    }
                },
                error: function (error) {
                    $('#divSuccessMsg').html('<div class="alert alert-danger js-message">'+ error.responseJSON.message +'</div>').hide()
                    
                    .fadeIn(1500, function() { $('#divSuccessMsg'); });
                   setTimeout(resetAll,3000);
                    hide_loading(self);
                }
            });
        }
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
     $(document).on('click', '.resendOtp', function (e) {
         
         var mobile = $('#mobile_no').val();
         
         let self = this;
         var url = window.location.href + '/login';
         
         show_loading(self);
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    mobile: mobile
                },
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (result) {
                    hide_loading(self);
                    
                    $('#divSuccessMsg').html('<div class="alert alert-success js-message">Success! OTP code has been resend to your mobile number.</div>').hide()
                    
                    .fadeIn(1500, function() { $('#divSuccessMsg'); });
                   setTimeout(resetAll,3000);
                    
                }
                
            })
         
     });
     
     
     $(document).on('submit', '#ChangePassword', function (e) {
        //  alert('as');
         
        e.preventDefault();
        let self = this;
        let is_success = validation(self);

        form_reset(self);

        if (is_success) {
            show_loading(self);
            $.ajax({
                url: $(self).attr('action'),
                type: 'POST',
                data: $(self).serialize(),
                success: function (result) {
                    // show_success_msg(self, result.message);
                    $('#divSuccessMsg').html('<div class="alert alert-success js-message">'+ result.message +'</div>').hide()
                    
                    .fadeIn(1500, function() { $('#divSuccessMsg'); });
                   
                    hide_loading(self);

                    if (result.auth) {
                        setTimeout(function () {
                            location.reload();
                        }, 10);
                    } else {
                        $(self).find('[type=submit]').html('Change Password');

                        $(self).find('.form-group').last().after(`
                            <div
                                class="form-group new-elements"
                            >
                                <label for="otp_code">OTP Code</label>
                                <input 
                                    type="tel" 
                                    minlength="6" 
                                    maxlength="6" 
                                    name="otp_code" 
                                    class="form-control"
                                    placeholder="Enter OTP Code"
                                    id="otp_code" 
                                    required
                                >
                            </div>
                            <div
                                class="form-group"
                            >
                                <label for="password">New Password *</label>
                                <input 
                                    type="password"
                                    minlength="8"
                                    maxlength="20"
                                    class="form-control"
                                    placeholder="Enter New Password"
                                    name="new_password"
                                    id="new_password"
                                    required
                                >
                            </div>
                            <div
                                class="form-group new-elements"
                            >
                                <label for="otp_code">Confirm Password</label>
                                <input 
                                    type="password"
                                    minlength="8"
                                    maxlength="20"
                                    class="form-control"
                                    placeholder="Enter Confirm Password"
                                    name="confirm_password"
                                    id="confirm_password"
                                    required
                                >
                            </div>
                            <div style="padding: 5px 0;">
                                <a class="resendOtp" style="color:#0000FF; cursor:pointer;margin-bottom:10px;">Resend Otp</a>
                            </div>
                        `);
                    }
                },
                error: function (error) {
                    $('#divSuccessMsg').html('<div class="alert alert-danger js-message">'+ error.responseJSON.message +'</div>').hide()
                    
                    .fadeIn(1500, function() { $('#divSuccessMsg'); });
                   setTimeout(resetAll,3000);
                    hide_loading(self);
                }
            });
        }
    });

    /**
     * Toggle Sidebar Menu
     */
    $(document).on('click', '.menu-sidebar ul>li.has-dropdown>a', function (e) {
        e.preventDefault();

        let sel = this;

        $('.menu-sidebar ul>li.has-dropdown:not(.active)>a').not(sel).parent().removeClass('open');
        $('.menu-sidebar ul>li.has-dropdown:not(.active)>a').not(sel).next().slideUp();
        $(sel).parent().not('.active').toggleClass('open');
        $(sel).parent().not('.active').find('ul').slideToggle();
    });

    $('.select2').select2();

    // $("[data-toggle='tooltip']").tooltip();

    $(document).on('click', '.delete-btn', function (e) {
        e.preventDefault();
        let text = $(this).data('text') ?? 'Once deleted, you will not be able to recover this imaginary file!';
        swal({
            title: "Are you sure?",
            text: text,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $('#deleteForm').attr('action', $(this).attr('href')).trigger('submit');
                }
            });
    });
});
