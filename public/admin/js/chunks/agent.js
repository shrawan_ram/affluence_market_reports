$(function () {
    /**
     * View Agent
     */
    let table_sel = $('.data-table');
    if (table_sel.length) {
        table_sel.DataTable({
            processing: true,
            serverSide: true,
            ajax: table_sel.data('ajax_url'),
            columns: [
                { "data": 'DT_RowIndex', "orderable": false, "searchable": false },
                // { "data": "id", "orderable": false, "searchable": false },
                { "data": "name" },
                { "data": "email" },
                { "data": "mobile" },
                { "data": "agent_sale_count" },
                { "data": "action", "orderable": false, "searchable": false },
            ],
        });
    }
});
