$(function () {
    /**
     * View Agent
     */
    let table_sel = $('.data-table');
    if (table_sel.length) {
        table_sel.DataTable({
            processing: true,
            serverSide: true,
            ajax: table_sel.data('ajax_url'),
            columns: [
                { "data": 'DT_RowIndex', "orderable": false, "searchable": false },
                { "data": "name" },
                { "data": "image" },
                { "data": "balance" },
                { "data": "stock_qty" },
                // { "data": "action", "orderable": false, "searchable": false },
            ],
        });
    }

    $(document).on('keyup', '#purchase_price, #margin', function () {
        let margin = $('#margin').val(),
            p_price = $('#purchase_price').val(),
            s_price = 0;

        margin = parseFloat(margin);
        p_price = parseFloat(p_price);

        s_price = p_price + (p_price * margin / 100);

        $('#sale_price').val(s_price);
    });
});
