<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('api/payment/status', 'agent\WalletController@paymentCallback');
Route::group(['as' => 'api.', 'namespace' => 'api'], function () {
    
    Route::apiResources([
        'state' => 'StateController',
        'city' => 'CityController',
        'agent' => 'AgentController',
        'supplier' => 'SupplierController',
        'item' => 'ItemController',
        'purchase' => 'PurchaseController',
        'sale' => 'SaleController',
        'stock' => 'StockController',
        'agent-sale' => 'AgentSaleController',
    ]);
});
Route::group(['prefix'=>'paypal'], function(){
    Route::post('/order/create',[\App\Http\Controllers\Front\PaypalPaymentController::class,'create']);
    Route::post('/order/capture/',[\App\Http\Controllers\Front\PaypalPaymentController::class,'capture']);
});