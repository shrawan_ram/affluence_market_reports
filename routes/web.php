<?php

use Illuminate\Support\Facades\Route;

Route::get('/clear', function () {
    return \Artisan::call('optimize:clear');
});

Route::get('/', 'HomeController@index');
Route::post('/newsletter', 'HomeController@newsletter');
Route::get('/category/{slug:slug}', 'CategoryController@single');
Route::get('/publisher', 'CategoryController@publisher');
Route::get('/publisher/{slug:slug}', 'CategoryController@publisher_single');
Route::get('thank-you', 'ReportController@thankyou')->name('thankyou');
Route::get('industry-analysis/checkout/{slug:id}', 'ReportController@get_checkout');
Route::post('industry-analysis/checkout-save', 'ReportController@save_checkout')->name('dopayment');
Route::get('/industry-analysis/{slug:id}', 'ReportController@report')->name('report_url');
Route::get('/latest-reports', 'ReportController@latest_reports');
Route::get('/page/{slug:slug}', 'PagesController@index');
Route::post('contact/ajax_request', 'AjaxController@index')->name('ajax-route');
Route::get('contact', 'ContactController@index');
Route::get('industry-analysis/{type}/{id}', 'ReportController@report_inquiry_form');
Route::post('industry-analysis', 'ReportController@report_inquiry_form_submit');
Route::get('reports', 'ReportController@searchReports')->name('search-reports');


Route::get('handle-payment', 'PayPalPaymentController@handlePayment')->name('make.payment');

Route::get('cancel-payment', 'PayPalPaymentController@paymentCancel')->name('cancel.payment');

Route::get('payment-success', 'PayPalPaymentController@paymentSuccess')->name('success.payment');


// Route::post('/', 'CheckoutController@save')->name('dopayment');



