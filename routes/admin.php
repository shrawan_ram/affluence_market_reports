<?php


use Illuminate\Support\Facades\Route;

// Login
    Route::get('admin-control/mobile', 'admin\MobileController@index');
    Route::get('admin-control/reset-password', 'admin\ResetpasswordController@index');

Route::group(['prefix' => 'admin-control', 'middleware' => 'guest'], function() {
    Route::get( '/', [ 'as' => 'login', 'uses' => 'admin\UserController@login'] );
    Route::post( 'login-auth', 'admin\UserController@auth' );
});

// Logout

Route::group(['prefix' => 'admin-control', 'middleware' => 'auth'], function() {
    Route::get( 'logout', 'admin\UserController@logout' );
    Route::get( 'dashboard', ['as' => 'home', 'uses' => 'admin\DashboardController@index' ]  );

// dashboard

    Route::get('dashboard','admin\DashboardController@index');
    Route::get('subscribe','admin\SubscribeController@index');
    Route::get('/contact_inquiry','admin\InquiryController@index');
    
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'clearede';
});





// Client

    Route::get('client', 'admin\ClientController@index'); 
    Route::get('client/add','admin\ClientController@add');
    Route::post('client/add','admin\ClientController@adddata');
    Route::get('client/edit/{id}', 'admin\ClientController@edit');
    Route::post('client/edit/{id}', 'admin\ClientController@editdata');
    Route::get('client/remove/{id}','admin\ClientController@remove');
    Route::post('client/removeMultiple','admin\ClientController@removeMultiple'); 



// Slider

    Route::get('slider', 'admin\SliderController@index');    
    Route::get('slider/add','admin\SliderController@add');
    Route::post('slider/add','admin\SliderController@adddata');
    Route::get('slider/edit/{id}', 'admin\SliderController@edit');
    Route::post('slider/edit/{id}', 'admin\SliderController@editdata');
    Route::get('slider/remove/{id}','admin\SliderController@remove');
    Route::post('slider/removeMultiple','admin\SliderController@removeMultiple');


// Setting

    Route::get('setting', 'admin\SettingController@index');
    Route::get('setting/edit/{id}', 'admin\SettingController@edit');
    Route::post('setting/edit/{id}', 'admin\SettingController@editdata');


// About us

    Route::get('page', 'admin\PageController@index');
    Route::get('page/add','admin\PageController@add');
    Route::post('page/add','admin\PageController@adddata');
    Route::get('page/edit/{id}', 'admin\PageController@edit');
    Route::post('page/edit/{id}', 'admin\PageController@editdata');
    Route::get('page/remove/{id}','admin\PageController@remove');
    Route::post('page/removeMultiple','admin\PageController@removeMultiple');


// category

    Route::get('category', 'admin\CategoryController@index');
    Route::get('category/add', 'admin\CategoryController@add');
    Route::post('category/add', 'admin\CategoryController@adddata');
    Route::get('category/edit/{id}', 'admin\CategoryController@edit');
    Route::post('category/edit/{id}', 'admin\CategoryController@editdata');
    Route::get('category/remove/{id}','admin\CategoryController@remove');
    Route::post('category/removeMultiple','admin\CategoryController@removeMultiple');

//

// publisher

Route::get('publisher', 'admin\PublisherController@index');
Route::get('publisher/add', 'admin\PublisherController@add');
Route::post('publisher/add', 'admin\PublisherController@adddata');
Route::get('publisher/edit/{id}', 'admin\PublisherController@edit');
Route::post('publisher/edit/{id}', 'admin\PublisherController@editdata');
Route::get('publisher/remove/{id}','admin\PublisherController@remove');
Route::post('publisher/removeMultiple','admin\PublisherController@removeMultiple');



// report

    Route::get('report', 'admin\ReportController@index');
    Route::get('report/add', 'admin\ReportController@add');
    Route::post('report/add', 'admin\ReportController@adddata');
    Route::get('report/edit/{id}', 'admin\ReportController@edit');
    Route::post('report/edit/{id}', 'admin\ReportController@editdata');
    Route::get('report/remove/{id}','admin\ReportController@remove');
    Route::post('report/removeMultiple','admin\ReportController@removeMultiple')->name('report.remove_multiple');
    Route::get('report/inquiry/{type:type}', 'admin\ReportController@show_inquiry');
    Route::get('report/orders', 'admin\ReportController@report_order');

    Route::post('/report/export', 'admin\ReportController@exportfile')->name('export');
    Route::get('importExportView', 'ReportController@importExportView');
    Route::post('report/import', 'admin\ReportController@importfile')->name('import');

//
   

// faq

    Route::get('faqs', 'admin\FaqsController@index');
    Route::get('faqs/add', 'admin\FaqsController@add');
    Route::post('faqs/add', 'admin\FaqsController@adddata');
    Route::get('faqs/edit/{id}', 'admin\FaqsController@edit');
    Route::post('faqs/edit/{id}', 'admin\FaqsController@editdata');
    Route::get('faqs/remove/{id}','admin\FaqsController@remove');
    Route::post('faqs/removeMultiple','admin\FaqsController@removeMultiple');
    


// change password

    Route::get('/change-password', 'admin\ChangepasswordController@index');
    Route::post('/change-password','admin\ChangepasswordController@changePassword');
});