<!DOCTYPE html>
<html>
<head>
	<title>{{ $subject }}</title>
	<style>
		table{
			width:100%;
			border-collapse: collapse;
		}
		table tr th, table tr td{
			border: 1px solid #ccc;
			padding: 5px 7px;
			vertical-align: baseline;
		}
	</style>
</head>
<body>
	<div style="max-width: 500px; margin: 15px auto;  font-family: Arial;">
		<!-- <div style="background: #fff; padding: 20px 15px; text-align: center">
			<img src="{{ url('imgs/logo.png') }}" alt="" style="width:50%;">
		</div>
		<div style="background: #79e6cf; padding: 15px;">
			<h2 style="font-weight: normal;">Thank You !!</h2>
			<p> Your Application has been Submitted Successfully.</p>
		</div> -->
		
		<table>
			<tr>
				<td colspan="2" style="background: rgba(6, 98, 178, 0.9) !important; color: #fff;">
					<!-- <div style="float:left;padding: 0px 30px;">
						<img src="{{url('imgs/'.$setting['logo'])}}" alt="" style="width:80px">
					</div> -->
					<div style="padding: 10px 10px;text-align:center">
						<h3>
							{{$setting['site_title']}}
						</h3>
						
					</div>
				</td>
			</tr>
			<tr>
				<th>Name</th>
				<td>{{$report['name']}}</td>
			</tr>
			<tr>
				<th>Mobile</th>
				<td>{{$report['mobile_no']}}</td>
			</tr>
			<tr>
				<th>Email</th>
				<td>{{$report['email']}}</td>
			</tr>
			<tr>
				<th>Country</th>
				<td>{{$country['name']}}</td>
			</tr>
			<tr>
				<th>Job Title</th>
				<td>{{$report['job']}}</td>
			</tr>
			<tr>
				<th>Company</th>
				<td>{{$report['company']}}</td>
			</tr>
			<tr>
				<th>Message</th>
				<td>{{$report['message']}}</td>
			</tr>
			<tr>
				<th>Report</th>
				<td>{{$report_detail['title']}}</td>
			</tr>
		</table>
	</div>
</body>
</html>
