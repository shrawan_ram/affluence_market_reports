@php
  $setting =  App\model\Setting::findOrFail(1);
@endphp
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ $setting->site_title }}</title>
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="icon">
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="apple-touch-icon">
  <!-- favicon-icon -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">        
  <!-- flaticon css -->
  <link rel="stylesheet" type="text/css" href="{{url('admin/css/flaticon.css')}}"/>
  <link href="{{url('admin/css/datepicker.css')}}" rel="stylesheet" type="text/css"/> 
  
  <link rel="stylesheet" href="{{url('admin/css/admin.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css"/> <!-- summernote css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/css/bootstrap-iconpicker.css" integrity="sha512-+pSMx/KtQ+OB6CetAEr/c1SLniitg+GUOXGqzRxBTePN0kl4WsMv//tXpFTm+lf3ZDIZ315m/soV+spYn5rITw==" crossorigin="anonymous" />
  <!-- Admin (main) Style Sheet -->
</head>
  <body class="hold-transition skin-blue">
<div class="wrapper">
  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('admin-control/')}}" class="logo" style="color: #000;">
      @if($setting->logo!='')
       <img src="{{ url('imgs/'.$setting->logo) }}" alt="{{ $setting->site_title }}">
       @else
       {{ $setting->site_title }}
       @endif
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <a href="{{url('/')}}" class="visit-site-btn btn" target="_blank">Visit Site <i class="material-icons right">keyboard_arrow_right</i></a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown admin-nav">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="material-icons">account_circle</i></button>
            <ul class="dropdown-menu animated flipInX">
              <li><a href="#">My Profile</a></li>
              <li>
                <a href="{{url('admin-control/logout')}}">
                    Logout
                </a>
                
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <i class="material-icons">account_circle</i>
        </div>
        <div class="pull-left info">
          <h4 class="user-name">{{auth()->user()->login}}</h4>
          <p>{{auth()->user()->user_name}}</p>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        @if(auth()->user()->role == 'admin')
        <li><a href="{{url('admin-control/')}}"><i class="material-icons">dashboard</i> <span>Dashboard</span></a></li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/page/add')}}"><i class="material-icons">label_outline</i> Add Page</a></li>
            <li><a href="{{url('admin-control/page/')}}"><i class="material-icons">label_outline</i>All Page</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Slider</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/slider/add')}}"><i class="material-icons">label_outline</i> Add Slider</a></li>
            <li><a href="{{url('admin-control/slider/')}}"><i class="material-icons">label_outline</i>All Slider</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/category/add')}}"><i class="material-icons">label_outline</i> Add Category</a></li>
            <li><a href="{{url('admin-control/category/')}}"><i class="material-icons">label_outline</i>All Category</a></li>
          </ul>
        </li>
        <!-- <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Publisher</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/publisher/add')}}"><i class="material-icons">label_outline</i> Add Publisher</a></li>
            <li><a href="{{url('admin-control/publisher/')}}"><i class="material-icons">label_outline</i>All Publisher</a></li>
          </ul>
        </li> -->
        
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/report/add')}}"><i class="material-icons">label_outline</i> Add Report</a></li>
            <li><a href="{{url('admin-control/report/')}}"><i class="material-icons">label_outline</i>All Report</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Our Client</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/client/add')}}"><i class="material-icons">label_outline</i> Add Client</a></li>
            <li><a href="{{url('admin-control/client/')}}"><i class="material-icons">label_outline</i>All Client</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">card_giftcard</i> <span>Faqs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/faqs/add')}}"><i class="material-icons">label_outline</i>Add Faq </a></li>
            <li><a href="{{url('admin-control/faqs/')}}"><i class="material-icons">label_outline</i>All Faq </a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">card_giftcard</i> <span>Report Inquiry</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/report/inquiry/sample')}}"><i class="material-icons">label_outline</i>Request Sample </a></li>
            <li><a href="{{url('admin-control/report/inquiry/discount')}}"><i class="material-icons">label_outline</i>Request Discount </a></li>
            <li><a href="{{url('admin-control/report/inquiry/buying')}}"><i class="material-icons">label_outline</i>Inquiry Before Buying </a></li>
            <li><a href="{{url('admin-control/report/inquiry/covid-19')}}"><i class="material-icons">label_outline</i>Covid-19</a></li>
          </ul>
        </li>
        <li><a href="{{url('admin-control/report/orders')}}"><i class="material-icons">dashboard</i> <span>Report Order</span></a></li>       
        <li><a href="{{url('admin-control/subscribe')}}"><i class="material-icons">dashboard</i> <span>Subscribers</span></a></li>       
        <li><a href="{{url('admin-control/contact_inquiry')}}"><i class="material-icons">dashboard</i> <span>Contact Inquiry</span></a></li>       
        
    
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">build</i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/setting/edit/1')}}"><i class="material-icons">label_outline</i> General Settings</a></li>
            <li><a href=""><i class="material-icons">label_outline</i>Social</a></li>
            <!-- <li><a href=""><i class="material-icons">label_outline</i>Footer</a></li> -->
          </ul>
        </li>
        @else 
        <li><a href="{{url('admin-control/report')}}"><i class="material-icons">dashboard</i> <span>Report</span></a></li>
        @endif
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <!-- Main content -->
    <section class="content container-fluid">
      @if (Session::has('added'))
        <div id="sessionModal" class="sessionmodal rgba-green-strong z-depth-2">
          <i class="fa fa-check-circle"></i> <p>{{session('added')}}</p>
        </div>
      @elseif (Session::has('updated'))
        <div id="sessionModal" class="sessionmodal rgba-cyan-strong z-depth-2">
          <i class="fa fa-check-circle"></i> <p>{{session('updated')}}</p>
        </div>
      @elseif (Session::has('deleted'))
        <div id="sessionModal" class="sessionmodal rgba-red-strong z-depth-2">
          <i class="fa fa-window-close"></i> <p>{{session('deleted')}}</p>
        </div>
      @endif
      @yield('contant')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
</div>
<!-- ./wrapper -->
<!-- Admin Js -->
<script src="{{url('admin/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/admin.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/summernote-bs4.min.js')}}"></script>
<!-- summernote js -->
<script src="{{url('admin/js/datatables.min.js')}}" type="text/javascript"></script>
    <!-- Datepicker Library -->
<script src="{{url('admin/js/datepicker.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/utils.js')}}" type="text/javascript"></script>
<script>
  $(function () {
    $('#flash-overlay-modal').modal();
    $('.alert').addClass('active');
    $('.alert').addClass('z-depth-1');
    setTimeout(function(){
      $('.alert:not(.alert-important)').removeClass('active');
    }, 6000);  
    $( '.date-picker' ).datepicker({
        format : "yyyy-mm-dd",
        startDate: '+1d',
       autoclose: true,
      });
    // DataTables
    $('#movies_table').DataTable({
      responsive: true,
      "sDom": "<'row'><'row'<'col-md-4'l><'col-md-4'B><'col-md-4'f>r>t<'row'<'col-sm-12'p>>",
      "language": {
        "paginate": {
          "previous": '<i class="material-icons paginate-btns">keyboard_arrow_left</i>',
          "next": '<i class="material-icons paginate-btns">keyboard_arrow_right</i>'
          }
      },
      buttons: [
        {
          extend: 'print',
          exportOptions: {
              columns: ':visible'
          }
        },
        'csvHtml5',
        'excelHtml5',
        'colvis',
      ]
    });

    $('#full_detail_table').DataTable({
      "sDom": "<'row'><'row'<'col-md-4'l><'col-md-4'B><'col-md-4'f>r>t<'row'<'col-sm-12'p>>",
      "aLengthMenu": [[100, 250, 500, 1000, -1], [100, 250, 500, 1000, "All"]],
      "pageLength": 100,
      "language": {
      "paginate": {
        "previous": '<i class="material-icons paginate-btns">keyboard_arrow_left</i>',
        "next": '<i class="material-icons paginate-btns">keyboard_arrow_right</i>'
        }
      },
      buttons: [
        {
          extend: 'print',
          exportOptions: {
              columns: ':visible'
          }
        },
        'csvHtml5',
        'excelHtml5',
        'colvis',
      ]
    });

    $('#summernote-main').summernote({
      height: 100,
    });

    $(".js-select2").select2({
        placeholder: "Pick states",
        theme: "material"
    });
    
    $(".select2-selection__arrow")
        .addClass("material-icons")
        .html("arrow_drop_down");
  });
</script>
<script>
  $(document).on('click','.btn-remove',function () {
        let form=$(this).closest('form');
        // alert($('.check:checked').length);
        if (form.find('.check:checked').length) {

           if (confirm('Are you sure you want to delete this item?')) {
            form.trigger('submit');
           }
       }else{
        alert('please select atleast one record to delete');
       }
    });
    
} );
} );
</script>
@yield('custom-script')
</body>
</html>

