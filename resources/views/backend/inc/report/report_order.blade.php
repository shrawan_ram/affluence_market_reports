@extends('backend.layout.master')

@section('title','inquiry')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a href="{{ url('admin-control/report/orders') }}" class="btn btn-danger btn-md">Report Orders</a>
      <!-- <a type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#bulk_delete"><i class="material-icons left">delete</i> Delete Selected</a>
         
      <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="delete-icon"></div>
            </div>
            <div class="modal-body text-center">
              <h4 class="modal-heading">Are You Sure ?</h4>
              <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
              {!! Form::open(['method' => 'POST', 'url' => url('admin-control/inquiry/removeMultiple') ,'id' => 'bulk_delete_form']) !!}
                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div> -->
      
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <!-- <div class="inline">
                <input id="checkboxAll" type="checkbox" class="filled-in check" name="checked[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div> -->
            #</th>
            <th>Name </th>
            <th>Email </th>
            <th>Mobile </th>
            <th>Job </th>
            <th>Comapny </th>
            <th>Address </th>
            <th>Report Name</th>
            <th>User Type</th>
            <th>Transaction Id </th>
            <th>Price </th>
            <th>Date</th>
            <!-- <th>Actions</th> -->
          </tr>
        </thead>
          @if (isset($reports))
          <tbody>
            @foreach ($reports as $key => $item)
              <tr>
                <td>
                  <!-- <div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="checked[]" value="{{$item->id}}" id="checkbox{{$item->id}}">
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div> -->
                  {{$key+1}}
                </td>
                <td>{{$item->fname}} {{ $item->lname }}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->mobile}}</td>
                <td>{{$item->job_title}}</td>
                <td>{{$item->company}}</td>
                <td>{{$item->address}}, {{$item->city}}, {{$item->state}}, {{$item->zip_code}}, {{ $item->countri ? $item->countri->name : 'NA'}}</td>
                <td>{{$item->report ? $item->report->title : 'NA'}}</td>
                <td>{{$item->user_type}}</td>
                <td>{{$item->transaction_id}}</td>
                <td>${{$item->price}}</td>
                <td>{{$item->created_at->format('d M Y')}}</td>
                <!-- <td>-
                  <div class="admin-table-action-block">
                    <a href="{{url('admin-control/inquiry/edit/'.$item->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                    
                    <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$item->id}}deleteModal"><i class="material-icons">delete</i> </button>
                    
                    <div id="{{$item->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                       
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                         <div class="modal-footer">
                            {!! Form::open(['url' => url('admin-control/inquiry/remove/'.$item->id),'method' => 'GET', $item->id]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td> -->
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>
@stop