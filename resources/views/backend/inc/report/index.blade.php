 @extends('backend.layout.master')

@section('title','report')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <div style="display: flex; justify-content: space-between;">
        <div>
          @if(auth()->user()->role == 'admin')<a href="{{ url('admin-control/report/add') }}" class="btn btn-danger btn-md">Add Report</a>@endif
        </div>
        <div>
          @if(auth()->user()->role == 'admin')<a type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#bulk_delete"><i class="material-icons left">delete</i> Delete Selected</a>@endif
        </div>
        <form action="" method="get">
        <div class="row">
              <div class="col-lg-4">  
                <label for="category">Category</label>
                <select name="category" class="form-control" id="category" style="margin-top:10px" >
                  <option value="">Select Category</option>
                  @foreach($categories as $c)
                  <option value="{{$c->id}}" @if(request('category') == $c->id ) selected @endif >{{ $c->title }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-3" >  
              <label for="from">From</label>
                <input type="date" name="from" id="from" value="{{request('from')}}" class="form-control" style="margin-bottom:0px">
              </div>
              <div class="col-lg-3"> 
              <label for="to">To</label> 
                <input type="date" name="to" id="to" class="form-control" value="{{request('to')}}" style="margin-bottom:0px">
              </div>
              <div class="col-lg-2">
                <input type="submit" class="btn btn-danger btn-md" value="Filter">
              </div>
            </div>
          </form>
      </div>
      <div class="row" style="margin-top:20px">
      <div class="col-lg-6"></div>
        <!-- <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data" style="background:red"> -->
        <form action="{{ route('export') }}"  id = 'bulk_delete_form' method="post">
          {{ csrf_field() }}
          
          <div class="col-lg-1" >
            <input type="hidden" name="category" value="{{request('category')}}">
            <input type="hidden" name="from" value="{{request('from')}}">
            <input type="hidden" name="to" value="{{request('from')}}">
           
            <input type="submit" class="btn btn-danger btn-md" value="Export XLSX" style="float:right">
          </div>

                    <!-- <a class="btn btn-warning" href="{{ route('export') }}">Export Bulk Data</a> -->

          <!-- Modal -->
          <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="delete-icon"></div>
                </div>
                <div class="modal-body text-center">
                  <h4 class="modal-heading">Are You Sure ?</h4>
                  <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                  <!-- {!! Form::open(['method' => 'POST', 'url' => url('admin-control/report/removeMultiple') ,'id' => 'bulk_delete_form']) !!} -->
                    <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-danger" onclick="deleteForm()">Yes</button>
                  <!-- {!! Form::close() !!} -->
                </div>
              </div>
            </div>
          </div>
        </form>
       
        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="col-lg-3" style="padding-right:40px">
            <input type="file" name="file" class="form-control" style="margin-top:20px;">
          </div>
          <div class="col-lg-2" >
            <input type="submit" class="btn btn-danger btn-md" value="Import XLSX" style="float:right;">
          </div>
        </form>

       

                
      </div>
         
      
    </div>
   
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <div class="inline">
                
                <input id="checkboxAll" type="checkbox" class="filled-in" name="checked[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div>
            #</th>
            <!-- <th>Image </th> -->
            <th>Title</th>
            <th>OMR Id</th>
            <th>Pages</th>
            <th>Single User</th>
            <th>Corporate User</th>
            <th>Cat Id</th>
            <th>Category</th>
            <th>Keyword</th>
            @if(auth()->user()->role == 'admin')
            <th>Actions</th>
            @endif
          </tr>
        </thead>
          @if (isset($report))
          <tbody>
            @foreach ($report as $key => $item)
              <tr>
                <td>
                @if(auth()->user()->role == 'admin')
                  <div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="checked[]" value="{{$item->id}}" id="checkbox{{$item->id}}">
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div>
                  @endif
                  {{$key+1}}
                </td>
                <!-- <td>
                  @if($setting->report_image == '')
                    <img src="{{ asset('imgs/default.jpg') }}" class="img-responsive" width="60" alt="image">
                  @else
                    <img src="{{ asset('imgs/'.$setting->report_image) }}" class="img-responsive" width="60" alt="image">                  
                  @endif
                </td> -->
                <td>{{$item->title}}</td>
                <td>{{$item->amr_id}}</td>
                <td>{{$item->pages}}</td>
                <td>${{$item->single_user}}</td>
                <td>${{$item->corporate_user}}</td>
                <td>{{ $item->category_id}}</td>
                <td>{{ $item->cat ? $item->cat->title : 'N/A'}}</td>
                <td>{{ $item->keyword }}</td>  
                <!-- <td>{{ $item->publisher ? $item->publisher->title : 'N/A'}}</td>   -->
                @if(auth()->user()->role == 'admin')
                <td>
                  <div class="admin-table-action-block">
                    <div style="display: flex;">
                      <a href="{{url('admin-control/report/edit/'.$item->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                      <!-- Delete Modal -->
                      <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$item->id}}deleteModal"><i class="material-icons">delete</i> </button>

                    </div>
                    <!-- Modal -->
                    <div id="{{$item->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
                            {!! Form::open(['url' => url('admin-control/report/remove/'.$item->id),'method' => 'GET', $item->id]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
                @endif
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>

  <script>
  function deleteForm()
  {
    var form = document.getElementById('bulk_delete_form');
    form.action = "{{ route('report.remove_multiple') }}";
    form.method = "POST";
    form.submit();
  }
  </script>
@stop