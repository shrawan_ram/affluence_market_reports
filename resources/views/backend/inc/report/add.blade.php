@extends('backend.layout.master')

@section('title','report')

@section('contant')

<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin-control/report')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Add report</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
          {!! Form::label('title', 'Report Title') !!} - <p class="inline info"></p>
          {!! Form::text('title', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('title') }}</small>
        </div> 
      </div>
      
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
          {!! Form::label('slug', 'Report Slug') !!} - <p class="inline info"></p>
          {!! Form::text('slug', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('slug') }}</small>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('keyword') ? ' has-error' : '' }}">
          {!! Form::label('keyword', 'Report keyword') !!} - <p class="inline info"></p>
          {!! Form::text('keyword', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('keyword') }}</small>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
          {!! Form::label('meta_title', 'Report Meta Title') !!} - <p class="inline info"></p>
          {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('meta_title') }}</small>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('pages') ? ' has-error' : '' }}">
          {!! Form::label('pages', 'Report Pages') !!} - <p class="inline info"></p>
          {!! Form::text('pages', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('pages') }}</small>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('single_user') ? ' has-error' : '' }}">
          {!! Form::label('single_user', 'Single User Price') !!} - <p class="inline info"></p>
          {!! Form::text('single_user', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('single_user') }}</small>
        </div> 
      </div>

      <div class="col-md-6">
        <div class="form-group{{ $errors->has('corporate_user') ? ' has-error' : '' }}">
          {!! Form::label('corporate_user', 'Corporate User Price') !!} - <p class="inline info"></p>
          {!! Form::text('corporate_user', null, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('corporate_user') }}</small>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
              {!! Form::label('category_id', 'Select Category') !!}            
              {{ Form::select('category_id', $parentArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('category_id') }}</small>
          </div>
      </div>

      <div class="clearfix"></div>

      <div class="col-md-6">
        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            {!! Form::label('created_at', 'Select Date') !!}            
            {{ Form::date('created_at', '', ['class' => 'form-control']) }}
            <small class="text-danger">{{ $errors->first('created_at') }}</small>
        </div>
      </div>
      <!-- <div class="col-md-6">
        <div class="form-group{{ $errors->has('publisher_id') ? ' has-error' : '' }}">
              {!! Form::label('publisher_id', 'Select Publisher') !!}            
              {{ Form::select('publisher_id', $publisherArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('publisher_id') }}</small>
          </div>
      </div> -->
        <div class="col-md-12">
          <div class="form-group{{ $errors->has('report_summery') ? ' has-error' : '' }}">
              {!! Form::label('report_summery', 'Report Summery') !!} - <p class="inline info"></p>
              {!! Form::textarea('report_summery', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('report_summery') }}</small>
          </div>
          <div class="summernote-main form-group{{ $errors->has('table_content') ? ' has-error' : '' }}">
            {!! Form::label('table_content', 'Table Content') !!}
            {!! Form::textarea('table_content', null, ['id' => 'summernote-main', 'class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('table_content') }}</small>
          </div>
          <!-- <div class="summernote-main form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['id' => 'summernote-main', 'class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('description') }}</small>
          </div> -->
          

          
		  
		  
     <!--     <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }} currency-symbol-block">
            {!! Form::label('icon', 'Category Icon / Symbol') !!}
            <p class="inline info"> - Please select catgeory symbol or category image</p>
              <div class="input-group">
                {!! Form::text('icon', null, ['class' => 'form-control category-icon-picker']) !!}
                <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
              </div>
            <small class="text-danger">{{ $errors->first('icon') }}</small>
          </div>
		  -->
		  
       
          <!-- <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} input-file-block">
            {!! Form::label('image', 'report Image') !!} 
            {!! Form::file('image', ['class' => 'input-file', 'id'=>'image']) !!}
            <label for="image" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="Category Image">
              <i class="icon fa fa-check"></i>
              <span class="js-fileName">Choose a File</span>
            </label>
            <p class="info">Choose custom image</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
          </div>  -->

          <!-- <h6 class="col-12"><p class="bg-primary text-white text-center p-4 font-weight-bold">Meta Info</p></h6>  

          <div class="col-md-6 form-group{{ $errors->has('seo_title') ? ' has-error' : '' }}">
              {!! Form::label('seo_title', 'Seo Title') !!} - <p class="inline info"></p>
              {!! Form::text('seo_title', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('seo_title') }}</small>
          </div>
          <div class="col-md-6 form-group{{ $errors->has('seo_keywords') ? ' has-error' : '' }}">
              {!! Form::label('seo_keywords', 'Seo Keywords') !!} - <p class="inline info"></p>
              {!! Form::text('seo_keywords', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('seo_keywords') }}</small>
          </div>
          <div class="form-group{{ $errors->has('seo_description') ? ' has-error' : '' }}">
              {!! Form::label('seo_description', 'Seo Description') !!} - <p class="inline info"></p>
              {!! Form::textarea('seo_description', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('seo_description') }}</small>
          </div> -->

          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop






