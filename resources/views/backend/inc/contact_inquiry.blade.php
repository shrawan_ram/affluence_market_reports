 @extends('backend.layout.master')

@section('title','contact inquiry')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a href="{{ url('admin-control/contact_inquiry') }}" class="btn btn-danger btn-md">Contact Inquiry</a>
      <!-- <a type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#bulk_delete"><i class="material-icons left">delete</i> Delete Selected</a>
         
      <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="delete-icon"></div>
            </div>
            <div class="modal-body text-center">
              <h4 class="modal-heading">Are You Sure ?</h4>
              <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
              {!! Form::open(['method' => 'POST', 'url' => url('admin-control/contact_inquiry/removeMultiple') ,'id' => 'bulk_delete_form']) !!}
                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div> -->
      
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <!-- <div class="inline">
                <input id="checkboxAll" type="checkbox" class="filled-in check" name="checked[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div> -->
            #</th>
            <th>Name </th>
            <th>Email </th>
            <th>Subject </th>
            <th>Message </th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
          @if (isset($contact_inquiry))
          <tbody>
            @foreach ($contact_inquiry as $key => $item)
              <tr>
                <td>
                  <!-- <div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="checked[]" value="{{$item->id}}" id="checkbox{{$item->id}}">
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div> -->
                  {{$key+1}}
                </td>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->subject}}</td>
                <td>{{$item->message}}</td>
                <td>{{$item->created_at->format('d M Y')}}</td>
                <td>-
                  <!-- <div class="admin-table-action-block">
                    <a href="{{url('admin-control/contact_inquiry/edit/'.$item->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                    
                    <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$item->id}}deleteModal"><i class="material-icons">delete</i> </button>
                    
                    <div id="{{$item->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                       
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                         <div class="modal-footer">
                            {!! Form::open(['url' => url('admin-control/contact_inquiry/remove/'.$item->id),'method' => 'GET', $item->id]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> -->
                </td>
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>
@stop