@extends('backend.layout.master')

@section('title','dashboard')

@section('contant')

@if(auth()->user()->role == 'admin')
<div class="content-main-block mrg-t-40">
  	<h4 class="admin-form-text">Dashboard</h4>
    <div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/slider')}}" class="small-box z-depth-1 hoverable bg-yellow secondary-color">
          <div class="inner">
            <h3>{{ $slider_count }}</h3>
            <p> Total Slider</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>

    	<div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/page')}}" class="small-box z-depth-1 hoverable bg-aqua default-color">
          <div class="inner">
            <h3>{{ $page_count }}</h3>
            <p>Pages</p>
          </div>
          <div class="icon">
            <i class="material-icons">card_membership</i>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/category')}}" class="small-box z-depth-1 hoverable bg-red danger-color">
          <div class="inner">
            <h3>{{ $category_count }}</h3>
            <p>Total Category</p>
          </div>
          <div class="icon">
            <i class="material-icons">card_membership</i>
          </div>
        </a>
      </div>      
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/report')}}" class="small-box z-depth-1 hoverable bg-green warning-color">
          <div class="inner">
            <h3>{{ $report_count }}</h3>
            <p>Total Reports</p>
          </div>
          <div class="icon">
            <i class="material-icons">card_giftcard</i>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/client')}}" class="small-box z-depth-1 hoverable bg-yellow secondary-color">
          <div class="inner">
            <h3>{{ $client_count }}</h3>
            <p> Total Client</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/inquiry')}}" class="small-box z-depth-1 hoverable bg-green success-color">
          <div class="inner">
            <h3>{{ $inquiry_count }}</h3>
            <p>Total Inquiry</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>
     
	  <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="{{url('admin-control/faqs')}}" class="small-box z-depth-1 hoverable bg-yellow secondary-color">
          <div class="inner">
            <h3>{{ $faq_count }}</h3>
            <p>Total Faqs</p>
          </div>
          <div class="icon">
            <i class="material-icons">monetization_on</i>
          </div>
        </a>
      </div>
	  <!--<div class="col-lg-3 col-xs-6">-->
        <!-- small box -->
   <!--     <a href="{{url('admin/coupon')}}" class="small-box z-depth-1 hoverable bg-aqua pink darken-2">-->
   <!--       <div class="inner">-->
   <!--         <h3>232</h3>-->
   <!--         <p>  Total Expired Coupon </p>-->
   <!--       </div>-->
   <!--       <div class="icon">-->
   <!--         <i class="material-icons">monetization_on</i>-->
   <!--       </div>-->
   <!--     </a>-->
   <!--   </div>-->
	  
    <!--  <div class="col-lg-3 col-xs-6">
        <!-- small box -->
       <!-- <a href="{{url('admin/discussion')}}" class="small-box z-depth-1 hoverable bg-aqua pink darken-2">
          <div class="inner">
            <h3></h3>
            <p>Total Discussion</p>
          </div>
          <div class="icon">
            <i class="material-icons">attach_money</i>
          </div>
        </a>
      </div>
	  -->
	  
	  
    </div>
  </div>
  @else 
  <div style="position:absolute;top:50%;left:50%;transform: translate(-50%, -50%);text-align:center">
  <!-- <i class="material-icons">Setting</i> -->
  <img src="{{url('imgs/welcome.png')}}"/>
    <h4 style="font-weight:bold">{{$setting->site_title}}</h2>
    <div> Sub Admin</div>
  </div>
  @endif

@stop
