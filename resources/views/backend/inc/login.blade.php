@extends('backend.layout.login-master')

@section('title','login')

@section('contant')

    <div class="limiter">
        @if( $errors->any() )
            <div class="alert alert-danger" style="color: red;">
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </div>
        @endif

        @if (\Session::has('success'))
            <div class="alert alert-success" style="color: green">
                {!! \Session::get('success') !!}</li>
            </div>
        @endif

        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
        <div class="container-login100" style="background-image: url('img/img-01.jpg');">
            <div class="wrap-login100 p-b-30">
                {{ Form::open( ['class'=>'login100-form validate-form', 'url' => url('admin-control/login-auth')] ) }}
                    <div class="login100-form-avatar">
                        <img src="{{ url('imgs/adminpng.png')}}" alt="AVATAR">
                    </div>

                    <span class="login100-form-title p-t-20 p-b-45">
                       Admin Login
                    </span>

                    <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
                        {{ Form::text('login', '', ['class' => 'form-control input100','login-input', 'required' => 'required','placeholder' => 'Username']) }}
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
                        {{ Form::password('password', ['class' => 'input100 form-control', 'required' => 'required','placeholder' => 'Password']) }}
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock"></i>
                        </span>
                    </div>

                    <div class="container-login100-form-btn p-t-10">
                        {{ Form::submit('LOGIN', ['class' => 'login-btn btn btn-warning text-secondary login100-form-btn']) }}
                    </div>

                    <!-- <div class="text-center w-full p-t-25 p-b-230">
                        <a href="#" class="txt1">
                            Forgot Username / Password?
                        </a>
                    </div>

                    <div class="text-center w-full">
                        <a class="txt1" href="#">
                            Create new account
                            <i class="fa fa-long-arrow-right"></i>                      
                        </a>
                    </div> -->
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop