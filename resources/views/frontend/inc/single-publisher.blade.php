@extends('frontend.layout.master')
@section('title','Publisher')
@section('contant')
<main id="main">
  <!-- ======= Breadcrumbs ======= -->
  <section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
      <ol>
        <li><a href="{{ url('/')}}">Home</a></li>
        <li><a href="{{ url('publisher')}}">Publisher</a></li>
        <li>{{ $slug->title }}</li>
      </ol>
    </div>
  </section><!-- End Breadcrumbs -->
  <!-- ======= Blog Section ======= -->
  <section id="blog" class="blog">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <h2 class="text-left pl-0 font-weight-bold"> {{ $slug->title }} </h2>
          <div id="why-choose">
            <div class="row d-flex align-items-center">
              <div class="col-lg-12">
                <!-- <h1 class="head-after">Latest Reports</h1> -->

                <div class="row">
                  <div class="col-lg-12">
                    @if($reports->count())
                    @foreach($reports as $report)
                    <div class="card">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="card-heading">
                            <a href="{{ url('industry-analysis',$report->id) }}" class="text-white">
                              <h4><strong>{{ $report->title }}</strong></h4>
                            </a>
                            @if($report->pages)
                            <p>No. of Pages: &nbsp;&nbsp; {{ $report->pages }} </p>
                            @endif
                          </div>
                          <div style="padding:15px">
                            <p><span style="display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden;">{{ $report->report_summery }}</span><span><a class="blc" href="{{ url('industry-analysis',$report->id) }}"><strong>Read More &gt;&gt;</strong></a></span></p><br>
                            <div class="blog-btn">
                              <h5>
                                <span class="text-left">Date: {{ date('M Y', strtotime($report->created_at)) }}</span>&nbsp;&nbsp;|
                                <span class="text-left">Price: ${{ $report->single_user }}</span>
                                <div class="float-right mb-3">
                                  <a class="btn request_sample_btn" href="{{ url('industry-analysis/sample/'.$report->id)}}">
                                    <svg class="svg-inline--fa fa-download fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                                    </svg>&nbsp;
                                    <strong>Request Sample </strong>
                                  </a>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <a class="btn buy_now_btn" href="{{ url('industry-analysis/checkout',$report->id) }}">
                                    <svg class="svg-inline--fa fa-shopping-cart fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-cart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"></path>
                                    </svg>
                                    <strong>Buy Now</strong>
                                  </a>
                                </div>
                              </h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    @else
                    <div class="card p-3">No record found.</div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 pl-0">
          <div class="sidebar" style="position: -webkit-sticky; position: sticky; top: 150px;padding:0">
            <div >
              <h3 class="sidebar-title" style>Reports</h3>
              <div class="sidebar-item recent-posts">
                @foreach( $category as $sr )
                <div class="post-item clearfix py-1">
                  @if($sr['image']!='')
                  <img src="{{ url('imgs/category/'.$sr['image'])}}" alt="{{ $sr['title'] }}">
                  @endif
                  @if($sr['image']!='')
                  <h4><a href="{{ url('publisher/'.$sr['slug'])}}" style="color:#1e67a8">{{ $sr['title'] }}</a></h4>
                  @else
                  <h4 style="margin-left:0px;"><a href="{{ url('publisher/'.$sr['slug'])}}" style="color: {{$sr['slug'] == $slug->slug ? '#1e67a8' : ''}}">{{ $sr['title'] }}</a></h4>
                  @endif
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- End Blog Section -->
</main><!-- End #main -->
@stop