@extends('frontend.layout.master')
@section('title','Contact')
@section('contant')
<main id="main">
  @php
  $payment = 0;
  $type = '';
  if(request('user')){
  if(request('user') == 'single'){
  $payment = $report->single_user;
  $type = 'single';
  }
  if(request('user') == 'corporate'){
  $payment = $report->corporate_user;
  $type = 'corporate';
  }
  } else {
  $payment = $report->single_user;
  $type = 'single';
  }
  @endphp
  <!-- ======= Breadcrumbs ======= -->
  <section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

      <ol>
        <li><a href="{{ url('/') }}">Home</a></li>
        <li><a href="{{ url('/industry-analysis',$report->id) }}">{{ $report->keyword }}</a></li>
        <li>Buy Now</li>
      </ol>

    </div>
  </section><!-- End Breadcrumbs -->

  <section id="blog" class="blog contact">
    <div class="container">
      {{ Form::open(['class'=>'php-email-form', 'id'=>'checkoutForm','method'=>'POST']) }}
      <div class="row">
        <div class="col-lg-7">
          <h2 class="text-left pl-0 font-weight-bold">Billing Detail</h2>
          <div class="form-group">
            <input type="text" name="fname" class="form-control" required id="fname" placeholder="First Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" name="lname" class="form-control" id="lname" placeholder="Last Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="email" required id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="number" class="form-control" name="mobile" required id="mobile" placeholder="Phone No.(Please Affix Country Code)" data-rule="minlen:10" data-msg="Please enter valid mobile number" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="job_title" required id="job_title" placeholder="Job title" data-rule="minlen:1" data-msg="Please enter job title" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="company" required id="company" placeholder="Company" data-rule="minlen:1" data-msg="Please enter company" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="address" id="address" required rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Address"></textarea>
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" name="city" class="form-control" id="city" required placeholder="City" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" name="state" class="form-control" id="state" required placeholder="State" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" name="zip_code" class="form-control" id="zip_code" required placeholder="Zip Code" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            {!! Form::select('country', $countryArr, null, ['class' => 'form-control','required']) !!}
            <div class="validate"></div>
          </div>
          <input type="hidden" value="{{ url('/') }}" id="base_url">
          <input type="hidden" name="transaction_id" id="paymentID" class="form-control" />
          <input type="hidden" name="report_id" value="{{ $report->id }}" class="form-control" />
          <input type="hidden" name="user_type" value="{{ $type }}" class="form-control" />
          <input type="hidden" name="price" value="{{ $payment }}" class="form-control" />
          <div class="mb-3">
            <div id="ajax_message" class="text-success"></div>
          </div>
        </div>
        <div class="col-lg-5 pl-0">
          <div class="sidebar" style="position: -webkit-sticky; position: sticky; top: 150px;padding:0">
            <div>
              <h3 class="sidebar-title">Shopping Cart Details</h3>
              <div class="sidebar-item recent-posts">

                <div class="post-item clearfix py-1">

                  <!-- <h4><a href="" style="color:red">ABCD</a></h4> -->
                  <div class="row">
                    <div class="col-3">
                      <h6 class="py-2 font-weight-bold" style="margin-left:0px;">Report</h6>
                    </div>
                    <div class="col-9">
                      <p>{{ $report->title }} {{ $report->title_suffix ? '-' : '' }} {{ $report->title_suffix }}</p>
                    </div>
                  </div>

                </div>
                <div class="post-item clearfix py-1">

                  <!-- <h4><a href="" style="color:red">ABCD</a></h4> -->
                  <div class="row">
                    <div class="col-3">
                      <h6 class="py-2 font-weight-bold" style="margin-left:0px;">AMR ID</h6>
                    </div>
                    <div class="col-9">
                      <p>{{ $report->amr_id ?? 'N/A' }}</p>
                    </div>
                  </div>

                </div>
                <div class="post-item clearfix py-1">

                  <!-- <h4><a href="" style="color:red">ABCD</a></h4> -->
                  <div class="row">
                    <div class="col-3">
                      <h6 class="py-2 font-weight-bold" style="margin-left:0px;">Licence Type</h6>
                    </div>
                    <div class="col-9">
                      @if(request('user'))
                      @if(request('user') == 'single')
                      <p>{{ $report->single_user }} $</p>
                      @endif
                      @if(request('user') == 'corporate')
                      <p>{{ $report->corporate_user }} $</p>
                      @endif
                      @else
                      <p>{{ $report->single_user }} $</p>
                      @endif
                    </div>
                  </div>

                </div>
                <div class="post-item clearfix pt-1 pb-3">
                  
                  <button type="submit" class="btn buy_now_btn" style="width:100%">
                    <strong>Pay with Razorpay &nbsp;&nbsp;
                      <svg class="svg-inline--fa fa-paper-plane fa-w-16" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="paper-plane" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                        <path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path>
                      </svg><!-- <i class="fa fa-paper-plane" aria-hidden="true"></i> --></strong>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{ Form::close() }}
    </div>
  </section>
 


</main>

@stop
@section('footer-script')
<script>
  
  // let baseUrl = window.location.origin + '/affluence_market_reports';
  let baseUrl = $('#base_url').val();
  // alert(baseUrl);
  console.log('url', baseUrl);
  $('#rzp-footer-form').submit(function(e) {
    var button = $(this).find('button');
    var parent = $(this);
    button.attr('disabled', 'true').html('Please Wait...');
    $.ajax({
      method: 'get',
      url: this.action,
      data: $(this).serialize(),
      complete: function(r) {
        console.log('complete');
        //console.log(r);
      }
    })
    return false;
  })
</script>

<script>
  function padStart(str) {
    return ('0' + str).slice(-2)
  }

  function demoSuccessHandler(transaction) {
    // alert('ab');
    let form = $('#checkoutForm');
    // You can write success code here. If you want to store some data in database.
    $("#paymentDetail").removeAttr('style');
    if (transaction) {
      console.log('url', transaction);
      $('#paymentID').val(transaction.razorpay_payment_id);
    }

    $.ajax({
      method: 'post',
      url: "{!! route('dopayment') !!}",
      data: form.serialize(),
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(r) {
        window.location = baseUrl + "/thank-you";
      }
    })
  }

  var options = {
    key: "{{ env('RAZORPAY_KEY') }}",
    amount: "{{ $payment * 100 }}",
    name: 'Affluence Market Reports',
    description: 'Affluence Market Reports',
    image: '{{ url("imgs/logo-amr.png") }}',
    currency: 'USD',
    handler: demoSuccessHandler
  }

  window.r = new Razorpay(options);

  $('#checkoutForm').on('submit', function(e) {
    e.preventDefault();
    is_valid = $(this).is_valid();
    if (is_valid) {
    let payMethod = $('.paymentMethod:checked').val();
    // if (payMethod == "RazorPay") {
    r.open();
    // } else {
    //   demoSuccessHandler(false);
    // }
    }
  });
</script>
@endsection