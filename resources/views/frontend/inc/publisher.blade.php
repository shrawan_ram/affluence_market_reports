@extends('frontend.layout.master')
@section('title','Publisher')
@section('contant')
<main id="main">
  <!-- <section> -->

  <div id="our-services" class="our-services-bg">
    <div class="container">
      <div class="text-center mb-5">
        <h3 class="head-after color-primary" style="color:#000000"><strong>Publisher</strong></h3>
      </div>
      <div class="row">
        @foreach($category as $c)
        <div class="col-lg-3 col-6">
          <div class='trand_report_cat_content mb-3'>
            <div class="mb-3 text-center">
              <a href="{{ url('publisher',$c->slug) }}">
                <div class="card-img-top img-hover-zoom">
                  <img title="{{ $c->title }}" alt="{{ $c->title }}" src="{{ url('imgs/category'.$c->image) }}" height="75px" width="80px">
                </div>
              </a>
            </div>
            <h3 class="text-center trand_cat_title">
              <a href="{{ url('publisher',$c->slug) }}" style="color:rgba(6, 98, 178, 0.9) !important;">
                <span><strong> {{$c->title}}</strong></span>
              </a>
            </h3>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
  <!-- </section> -->
</main>
@stop