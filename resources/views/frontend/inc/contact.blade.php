@extends('frontend.layout.master')

@section('title','Contact')

@section('contant')
<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  <section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

      <ol>
        <li><a href="{{ url('/') }}">Home</a></li>
        <li>Contact Us</li>
      </ol>

    </div>
  </section><!-- End Breadcrumbs -->

  <section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

      <h2 class="text-left pl-0 font-weight-bold"> Contact Us</h2>
      <!-- <div class="section-title">
        </div> -->

      <div class="my-1 row justify-content-end" data-aos="fade-right" data-aos-delay="100">

        <div class="col-lg-6">
          <div class="info">
            <div class="address">
              <i class="icofont-google-map"></i>
              <h4>Location:</h4>
              <p>{{ $setting->address }}</p>
            </div>

            <div class="email">
              <i class="icofont-envelope"></i>
              <h4>Email:</h4>
              <p><a href="mailto:{{ $setting->email }}" style="color: #000">{{ $setting->email }}</a></p>
            </div>

            <div class="phone">
              <i class="icofont-phone"></i>
              <h4>Call:</h4>
              <p><a href="tel:{{ $setting->email }}" style="color: #000">{{ $setting->mobile }}</a></p>
            </div>

          </div>

        </div>

        <div class="col-lg-6 my-5 mt-lg-0" data-aos="fade-left" data-aos-delay="100">

          {{ Form::open(['class'=>'php-email-form', 'id'=>'inquiry_form', 'data-url'=>route('ajax-route')]) }}
          <div class="form-row">
            <div class="col-md-6 form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validate"></div>
            </div>
            <div class="col-md-6 form-group">
              <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
              <div class="validate"></div>
            </div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="message" id="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
            <div class="validate"></div>
          </div>
          <div class="mb-3">

            <div id="ajax_message" class="text-success"></div>
          </div>
          <div class="text-center"><button type="submit" style="outline: 0;">Send Message</button></div>
          {{ Form::close() }}

        </div>

      </div>

    </div>
  </section>

</main><!-- End #main -->
@stop