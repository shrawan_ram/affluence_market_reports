@extends('frontend.layout.master')

@section('title','Homepage')

@section('contant')

<section id="" class="justify-cntent-center align-items-center">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">

      @foreach($slider as $key => $item)

      <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="{{ $key == 0 ? 'active' : ''}}"></li>
      @endforeach

    </ol>
    <div class="carousel-inner">
      @foreach($slider as $key => $item)
      <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
        <img class="d-block w-100" src="{{ url('imgs/slider/'.$item->image) }}" alt="First slide">
        <div class="slider_overlay">
          <h1>{{ $item->title }}</h1>
          <div style="color:#fff!important">{!! $item->description !!}</div>
        </div>
      </div>

      @endforeach

      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</section><!-- End Hero -->


<div id="our-services" class="our-services-bg">
  <div class="container">
    <div class="text-center mb-5">
      <h3 class="head-after color-primary" style="color:#000000"><strong>Trending Reports By Industries 
        <!-- <a href="{{ route('make.payment') }}" class="btn btn-primary mt-3">Pay $224 via Paypal</a> -->
      </strong></h3>
    </div>
    <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
      <input type="hidden" name="cmd" value="_xclick">
      <input type="hidden" name="business" value="sb-bwnna3501319@business.example.com"> 
      <input type="hidden" name="item_name" value="ram">
      <input type="hidden" name="item_number" value="12">

      <input type="hidden" name="amount" value="999">
      <input type="hidden" name="currency_code" value="USD">
      <input type="hidden" name="return" value="https://www.facebook.com/">
      <input type="hidden" name="cancel_return" value="https://www.facebook.com/"> 
      <input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif">
    </form> -->
    <div class="row">
      @foreach($category as $c)
      <div class="col-lg-3 col-6">
        <div class='trand_report_cat_content mb-3'>
          <div class="mb-3 text-center">
            <a href="{{ url('category',$c->slug) }}">
              <div class="card-img-top img-hover-zoom">
                <img title="{{ $c->title }}" alt="{{ $c->title }}" src="{{ url('imgs/category/'.$c->image) }}" height="75px" width="80px">
              </div>
            </a>
          </div>
          <h3 class="text-center trand_cat_title">
            <a href="{{ url('category',$c->slug) }}" style="color:#450461 !important;">
              <span><strong> {{$c->title}}</strong></span>
            </a>
          </h3>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

<div id="our-services">
  <div class="container">
    <div class="text-center mb-5">
      <h3 class="head-after color-primary" style="color:#000000"><strong>Our Clients</strong></h3>
    </div>
    <section class="customer-logos slider">
      @foreach($clients as $client)
      <div class="slide thumbnail"><img alt="{{ $client->name }}" title="{{ $client->name }}" src="{{ url('imgs/clients/'.$client->image) }}"></div>
      @endforeach
    </section>
  </div>
</div>
<div id="why-choose" class="our-services-bg">
  <div class="container">
    <div class="text-center mb-5">
      <h3 class="head-after color-primary" style="color:#000000"><strong>Latest Reports</strong></h3>
    </div>
    <div class="row d-flex align-items-center">
      <div class="col-lg-12">
        <!-- <h1 class="head-after">Latest Reports</h1> -->

        <div class="row">
          <div class="col-lg-12">
            @foreach($reports as $report)
            <div class="card">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-heading">
                    <a class="text-white" href="{{ url('industry-analysis',$report->id) }}">
                      <h4><strong>{{ $report->title }}</strong></h4>
                    </a>
                    @if($report->pages)
                    <p>No. of Pages: &nbsp;&nbsp; {{ $report->pages }} </p>
                    @endif
                  </div>
                  <div style="padding:15px">
                    <p><span style="display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden;">{{ $report->report_summery }}</span><span><a class="blc" href="{{ url('industry-analysis',$report->id) }}"><strong>Read More &gt;&gt;</strong></a></span></p><br>
                    <div class="blog-btn">
                      <h5>
                        <span class="text-left">Date: {{ date('M Y', strtotime($report->created_at)) }}</span>&nbsp;&nbsp;|
                        <span class="text-left">Price: ${{ $report->single_user }}</span>
                        <div class="float-right mb-3">
                          <a class="btn request_sample_btn" href="{{ url('industry-analysis/sample/'.$report->id)}}">
                            <svg class="svg-inline--fa fa-download fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                              <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                            </svg>&nbsp;
                            <strong>Request Sample </strong>
                          </a>
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          <a class="btn buy_now_btn" href="{{ url('industry-analysis/checkout',$report->id) }}">
                            <svg class="svg-inline--fa fa-shopping-cart fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-cart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                              <path fill="currentColor" d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"></path>
                            </svg>
                            <strong>Buy Now</strong>
                          </a>
                        </div>
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="txt-center mb-xs-3 " style="margin:auto">
            <a href="{{ url('latest-reports') }}"><button type="submit" name="submit" class="btn request_sample_btn"><strong>View More Publications</strong></button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- ======= Frequently Asked Questions Section ======= -->
<!-- <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
        </div>

        <div class="faq-list">
          <ul>
            @foreach( $faq as $item )
            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-{{ $item->id }}" class="collapsed">{{ $item->question }}<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-{{ $item->id }}" class="collapse" data-parent=".faq-list">
                <p>
                 {!! $item->answer !!}
                </p>
              </div>
            </li>
            @endforeach
          </ul>
        </div>

      </div>
    </section> -->
<!-- End Frequently Asked Questions Section -->
<script>
  $(document).ready(function() {
    $('.customer-logos').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1500,
      arrows: false,
      dots: false,
      pauseOnHover: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      }, {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }]
    });
  });
</script>
@stop