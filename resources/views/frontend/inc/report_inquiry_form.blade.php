@extends('frontend.layout.master')

@section('title','Contact')

@section('contant')
<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  <section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

      <ol>
        <li><a href="{{ url('/') }}">Home</a></li>
        <li><a href="{{ url('industry-analysis',$report->id) }}">{{ $report->keyword }}</a></li>
        @if($type == 'discount')
        <li>Request Discount</li>
        @endif
        @if($type == 'sample')
        <li>Request Sample</li>
        @endif
        @if($type == 'buying')
        <li>Inquery Before Buying</li>
        @endif
        @if($type == 'covid-19')
        <li>COVID 19 Impact</li>
        @endif
      </ol>
    </div>
  </section><!-- End Breadcrumbs -->

  <section id="blog" class="blog contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 pb-3">
          <h2 class="text-left pl-0 font-weight-bold">{{ $report->title }}</h2>
          {{ Form::open(['class'=>'php-email-form','action'=>'ReportController@report_inquiry_form_submit', 'method'=>'POST']) }}
          <div class="form-row">
            <input type="hidden" class="form-control" name="type" value="{{ $type }}" />
            <input type="hidden" class="form-control" name="report_id" value="{{ $report->id }}" />
            <div class="col-md-6 form-group">
              <input type="text" name="name" required class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validate"></div>
            </div>
            <div class="col-md-6 form-group">
              <input type="email" class="form-control" required name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
              <div class="validate"></div>
            </div>
          </div>
          <div class="form-group">
            {!! Form::select('country', $countryArr, null, ['class' => 'form-control','required']) !!}
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="number" class="form-control" minlength="10" name="mobile_no" required id="mobile" placeholder="Phone No.(Please Affix Country Code)" data-rule="minlen:10" data-msg="Please enter valid mobile number" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="job" required id="job_title" placeholder="Job title" data-rule="minlen:1" data-msg="Please enter job title" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="company" required id="company" placeholder="Company" data-rule="minlen:1" data-msg="Please enter company" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="message" id="message" rows="5" placeholder="Please specify your requirement to help you with all the research solutions."></textarea>
            <div class="validate"></div>
          </div>
          <div class="mb-3">
            <div id="ajax_message" class="text-success"></div>
          </div>
          <div class="text-center"><button type="submit" style="outline: 0;border-radius:5px">Submit</button></div>
          {{ Form::close() }}
        </div>
        <div class="col-lg-3 pl-0">
          <div style="position: -webkit-sticky; position: sticky; top: 135px;">
            <div class="sidebar" style="padding:0">
              <div>
                <h3 class="sidebar-title">Why Choose us?</h3>
                <div class="sidebar-item recent-posts">
                  <div class="post-item clearfix py-1">
                    <h6 class="py-2" style="margin-left:0px;">24/7 Research support</h6>
                    <h6 class="py-2" style="margin-left:0px;">Quality assurance</h6>
                    <h6 class="py-2" style="margin-left:0px;">Information security</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="sidebar" style="padding:0">
              <div>
                <h3 class="sidebar-title">Support</h3>
                <div class="sidebar-item recent-posts">
                  <div class="post-item clearfix py-1">
                    <h6 class="py-2" style="margin-left:0px;"><a href="tel:{{ $setting->mobile }}" style="color: #000"><i class="icofont-phone"></i> {{ $setting->mobile }}</a></h6>
                    <h6 class="py-2" style="margin-left:0px;"><a href="mailto:{{ $setting->email }}" style="color: #000"><i class="icofont-envelope"></i> {{ $setting->email }}</a></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


</main><!-- End #main -->
@stop