@extends('frontend.layout.master')
@section('title','Thank You')
@section('contant')
<main id="main">
  <!-- ======= Blog Section ======= -->
  <section id="blog" class="blog">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="why-choose">
            <div class="">
              <div style="text-align:center!important;">
                <h1 style="font-size:60px">Thank You!!</h1>
                <!-- <i class="fa fa-check text-success" style="font-size:30px"></i> -->
                <p style="font-size:25px">

                  Our sales representitive will soon get back to you !!
                </p>
                <p style="font-size:25px">Having Trouble? <a href="{{ url('contact') }}" style="font-size:25px">Contact Us</a></p>
                <a class="btn request_sample_btn" href="{{ url('/')}}">
                  <strong>Continue to homepage</strong>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- End Blog Section -->
</main><!-- End #main -->
@stop