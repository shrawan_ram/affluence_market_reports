@extends('frontend.layout.master')
@section('title','Report')
@section('contant')
<header id="header" class=" header-inner-pages report_header d-none" style="position:sticky;top:0">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="{{ url('/') }}" class="scrollto">

          @if(!empty($setting->logo))
          <img src="{{ url('imgs/'.$setting->logo) }}" alt="logo">
          @else
          {{ $setting->site_title }}
          @endif
        </a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      <button class="navbar-toggler d-sm-block d-lg-none" type="button" id="nav_button">
        <span class="navbar-toggler-icon"><i class="fa fa-bars text-white"></i></span>
      </button>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="">
          <a href="{{ url('industry-analysis/sample/'.$report->id)}}">
              <button class="btn btn-success">
                <span>
                  <svg class="svg-inline--fa fa-download fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                    <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                  </svg>
                  &nbsp; <strong>Request Sample </strong>
                </span>
              </button>
            </a>
          </li>
          
          <li>
            <a href="{{ url('industry-analysis/discount/'.$report->id)}}">
              <button class="btn btn-danger">
                <span>
                  <svg class="svg-inline--fa fa-percentage fa-w-12" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="percentage" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg="">
                    <path fill="currentColor" d="M109.25 173.25c24.99-24.99 24.99-65.52 0-90.51-24.99-24.99-65.52-24.99-90.51 0-24.99 24.99-24.99 65.52 0 90.51 25 25 65.52 25 90.51 0zm256 165.49c-24.99-24.99-65.52-24.99-90.51 0-24.99 24.99-24.99 65.52 0 90.51 24.99 24.99 65.52 24.99 90.51 0 25-24.99 25-65.51 0-90.51zm-1.94-231.43l-22.62-22.62c-12.5-12.5-32.76-12.5-45.25 0L20.69 359.44c-12.5 12.5-12.5 32.76 0 45.25l22.62 22.62c12.5 12.5 32.76 12.5 45.25 0l274.75-274.75c12.5-12.49 12.5-32.75 0-45.25z"></path>
                  </svg>
                  &nbsp; <strong>Request Discount </strong>
                </span>
              </button>
            </a>
          </li>
          <li>
            <a href="{{ url('industry-analysis/buying/'.$report->id)}}">
              <b>
                <button class="btn btn-info">
                  <span>
                    <svg class="svg-inline--fa fa-headset fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="headset" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                      <path fill="currentColor" d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z"></path>
                    </svg>
                    &nbsp; <strong>Inquire Before Buying</strong>
                  </span></button>
              </b>
            </a>
          </li>
          <!--<li><a href="{{ url('/ourteam')}}">Team</a></li>-->

          <!-- <li><a href="{{ url('/contact') }}">Contact</a></li>
            <li class="d-xs-block d-sm-block d-md-block d-lg-none d-xl-none"><a href="{{ url('/contact') }}">Login</a></li> -->
        </ul>
      </nav><!-- .nav-menu -->

    </div>
</header>
<main id="main">
  <!-- ======= Breadcrumbs ======= -->
  <section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
      <ol>
        <li><a href="{{ url('/')}}">Home</a></li>
        <li>{{ $report->keyword }}</li>
      </ol>
    </div>
  </section><!-- End Breadcrumbs -->
  <!-- ======= Blog Section ======= -->
  <section id="blog" class="blog">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <!-- <h2 class="text-left pl-0 font-weight-bold"> {{ $report->title }} {{ $report->title_suffix ? '-' : '' }} {{ $report->title_suffix }} </h2> -->
          <div id="why-choose">
            <div class="media ">
              <img src="{{ url('imgs/',$setting->report_image) }}" height="200px" width="160px" alt="Market Research Report" title="Market Research Report" class="align-self-start mr-4 d-none d-sm-block img-responsive">
              <div class="media-body">
                <h1 class="rd-page-title">{{ $report->title }} </h1>
                <ul class="list-inline rd-cat">
                  @if($report->amr_id)
                  <li class="list-inline-item"><b>OMR ID</b> : {{ $report->amr_id }}</li>
                  @endif
                  @if($report->category_id)
                  <li class="list-inline-item"><b>CAT ID</b> : {{ $report->category_id }}</li>
                  @endif
                  @if($report->pages)
                  <li class="list-inline-item"><b>Pages</b> : {{ $report->pages }}</li>
                  @endif
                  <li class="list-inline-item"><b>Date</b> : {{ date('M Y', strtotime($report->created_at)) }}</li>

                  <!--		<li><span><b>Publisher</b> : Market Monitor Global</span></li>-->
                </ul>
                <div id="calltoac">
                  <a href="{{ url('industry-analysis/sample',$report->id)}}">
                    <button class="btn btn-success">
                      <span>
                        <svg class="svg-inline--fa fa-download fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                          <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                        </svg>
                        &nbsp; <strong>Request Sample </strong>
                      </span>
                    </button>
                  </a>
                  <a href="{{ url('industry-analysis/discount/'.$report->id)}}">
                    <button class="btn btn-danger">
                      <span>
                        <svg class="svg-inline--fa fa-percentage fa-w-12" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="percentage" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg="">
                          <path fill="currentColor" d="M109.25 173.25c24.99-24.99 24.99-65.52 0-90.51-24.99-24.99-65.52-24.99-90.51 0-24.99 24.99-24.99 65.52 0 90.51 25 25 65.52 25 90.51 0zm256 165.49c-24.99-24.99-65.52-24.99-90.51 0-24.99 24.99-24.99 65.52 0 90.51 24.99 24.99 65.52 24.99 90.51 0 25-24.99 25-65.51 0-90.51zm-1.94-231.43l-22.62-22.62c-12.5-12.5-32.76-12.5-45.25 0L20.69 359.44c-12.5 12.5-12.5 32.76 0 45.25l22.62 22.62c12.5 12.5 32.76 12.5 45.25 0l274.75-274.75c12.5-12.49 12.5-32.75 0-45.25z"></path>
                        </svg>
                        &nbsp; <strong>Request Discount </strong>
                      </span>
                    </button>
                  </a>
                  <a href="{{ url('industry-analysis/buying/'.$report->id)}}">
                    <b>
                      <button class="btn btn-info">
                        <span>
                          <svg class="svg-inline--fa fa-headset fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="headset" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                            <path fill="currentColor" d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z"></path>
                          </svg>
                          &nbsp; <strong>Inquire Before Buying</strong>
                        </span></button>
                    </b>
                  </a>
                  <a href="{{ url('industry-analysis/covid-19/'.$report->id)}}">
                    <button class="btn btn-danger" style="background-color:#ff0000;border:none;">
                      <span> &nbsp;
                        <strong>COVID-19 Impact Analysis</strong>
                      </span>
                    </button>
                  </a>
                </div>
              </div>
            </div>
            <nav class="my-4">
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Report Summary</button>
                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Table of Content</button>
                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Related Reports</button>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">{!! nl2br($report->report_summery) !!}</div>
              <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">{!! nl2br($report->table_content) !!}</div>
              <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                @foreach($related['data'] as $rr )
                <div class="card">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="card-heading">
                        <a class="text-white" href="{{ url('industry-analysis',$rr['id']) }}">
                          <h4><strong>{{ $rr['title'] }}</strong></h4>
                        </a>
                        @if($rr['pages'])
                        <p>No. of Pages: &nbsp;&nbsp; {{ $rr['pages'] }} </p>
                        @endif
                        
                      </div>
                      <div style="padding:15px">
                        <p><span style="display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden;">{{ $rr['report_summery'] }}</span><span><a class="blc" href="{{ url('industry-analysis',$rr['id']) }}"><strong>Read More &gt;&gt;</strong></a></span></p><br>
                        <div class="blog-btn">
                          <h5>
                            <span class="text-left">Date: {{ date('M Y', strtotime($rr['created_at'])) }}</span>&nbsp;&nbsp;|
                            <span class="text-left">Price:${{ $rr['single_user'] }}</span>
                            <div class="float-right mb-3">
                              <a class="btn request_sample_btn" href="{{ url('industry-analysis/sample/'.$rr['id'])}}">
                                <svg class="svg-inline--fa fa-download fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                  <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                                </svg>&nbsp;
                                <strong>Request Sample </strong>
                              </a>
                              &nbsp;&nbsp;&nbsp;&nbsp;
                              <a class="btn buy_now_btn" href="{{ url('industry-analysis/checkout',$rr['id']) }}">
                                <svg class="svg-inline--fa fa-shopping-cart fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-cart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                  <path fill="currentColor" d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"></path>
                                </svg>
                                <strong>Buy Now</strong>
                              </a>
                            </div>
                          </h5>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                

              </div>
            </div>
            <!-- <h2>Report Summery</h2> -->
            
          </div>
        </div>
        <div class="col-lg-3 pl-0">
          <div class="sidebar" style="padding:0">
            <div>
              <h3 class="sidebar-title">Choose License Type</h3>
              <div class="sidebar-item recent-posts">

                <div class="post-item clearfix py-1">
                  <form action="{{ url('industry-analysis/checkout',$report->id) }}">
                    <div class="post-item clearfix">
                      <input type=radio name="user" value="single" checked> Single User : <strong> ${{ $report->single_user }}</strong>
                    </div>
                    <div class="post-item clearfix">
                      <input type=radio name="user" value="corporate"> Corporate User : <strong> ${{ $report->corporate_user }}</strong>
                    </div>
                    <div class="post-item">
                      <button type="submit" class="btn buy_now_btn" style="width:100%">
                        <svg class="svg-inline--fa fa-shopping-cart fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-cart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                          <path fill="currentColor" d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"></path>
                        </svg>&nbsp;
                        <strong>Buy Now</strong>
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="sidebar" style="position: -webkit-sticky; position: sticky !important; top: 150px;padding:0">
            <div>
              <h6 class="sidebar-title" style> Request Sample Report</h6>
              <div class="sidebar-item recent-posts">
                <div class="post-item clearfix">
                {{ Form::open(['class'=>'php-email-form','action'=>'ReportController@report_inquiry_form_submit', 'method'=>'POST']) }}
                  
                    <input type="hidden" class="form-control" name="type" value="sample" />
                    <input type="hidden" class="form-control" name="report_id" value="{{ $report->id }}" />
                    <div class="form-group">
                      <input type="text" name="name" required class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                      <div class="validate"></div>
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                      <div class="validate"></div>
                    </div>
                
                  <div class="form-group">
                    {!! Form::select('country', $countryArr, null, ['class' => 'form-control','required']) !!}
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" minlength="10" name="mobile_no" required id="mobile" placeholder="Phone No.(Please Affix Country Code)" data-rule="minlen:10" data-msg="Please enter valid mobile number" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="job" required id="job_title" placeholder="Job title" data-rule="minlen:1" data-msg="Please enter job title" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="company" required id="company" placeholder="Company" data-rule="minlen:1" data-msg="Please enter company" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" name="message" id="message" rows="5" placeholder="Please specify your requirement to help you with all the research solutions."></textarea>
                    <div class="validate"></div>
                  </div>
                  <div class="mb-3">
                    <div id="ajax_message" class="text-success"></div>
                  </div>
                  <div class="text-center"><button type="submit" class="btn request_sample_btn" style="outline: 0;border-radius:5px; width:100%">Submit</button></div>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section><!-- End Blog Section -->
  <section id="faq" class="faq section-bg pb-2">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Frequently Asked Questions</h2>
      </div>

      <div class="faq-list">
        <ul>
          @foreach( $faq as $item )
          <li data-aos="fade-up" data-aos-delay="200">
            <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-{{ $item->id }}" class="collapsed">{{ $item->question }}<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="faq-list-{{ $item->id }}" class="collapse" data-parent=".faq-list">
              <p>
              {!! $item->answer !!}
              </p>
            </div>
          </li>
          @endforeach
        </ul>
      </div>

    </div>
  </section>
</main><!-- End #main -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
  $(window).scroll(function() {
 if ($(this).scrollTop() > 100){
  //  alert('ab');  
    $('.header_hide').hide();
    $('.report_header').show();
    $('.report_header').addClass('d-block');
  }
  else{
    $('.header_hide').show();
    $('.report_header').hide();
    $('.report_header').removeClass('d-block');
  }
});
</script>
@stop