@extends('frontend.layout.master')

@section('title','Pages')

@section('contant')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{ url('/') }}">Home</a></li>
          <li>{{$slug->title}}</li>
        </ol>
        <!-- <h2>About Us</h2> -->

      </div>
    </section><!-- End Breadcrumbs -->

     <section id="about" class="about">
      <div class="container" data-aos="fade-up">

      <h2 class="text-left pl-0 font-weight-bold"> {{$slug->title}}  </h2>        
        <div>
          {!! $slug->description !!}
        </div>
            
         

      </div>
    </section>

  </main>
@stop