<?php
$setting = App\model\Setting::findOrFail(1);
$category = App\model\Category::where('type','category')->orderBy('title')->get();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $setting->site_title }}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <base href="{{ url('/') }}">

  <!-- Favicons -->
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="icon">
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="apple-touch-icon">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
  {{Html::style('vendor/bootstrap/css/bootstrap.min.css')}}
  {{Html::style('vendor/icofont/icofont.min.css')}}
  {{Html::style('vendor/boxicons/css/boxicons.min.css')}}
  {{Html::style('vendor/animate.css/animate.min.css')}}
  {{Html::style('vendor/remixicon/remixicon.css')}}
  {{Html::style('vendor/owl.carousel/assets/owl.carousel.min.css')}}
  {{Html::style('vendor/venobox/venobox.css')}}
  <!--{{Html::style('vendor/aos/aos.css')}}-->
  {{Html::style('css/style.css')}}
  {{Html::style('css/bootstrap-pincode-input.css')}}

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    #main {
      min-height: calc(100vh - 590px);
    }
  </style>


</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
        <ul>
          <li><i class="icofont-envelope"></i><a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a></li>
          <li><i class="icofont-phone"></i><a href="tel:{{ $setting->mobile }}">{{ $setting->mobile }}</a></li>
          <!-- <li><i class="icofont-clock-time icofont-flip-horizontal"></i> Mon-Fri 9am - 5pm</li> -->
        </ul>
      </div>

      <div class="contact-info mr-auto" style="margin-right: 0 !important;">
        



      </div>
      <!-- <div class="cta">
        <a href="{{ url('/agent-panel') }}" class="scrollto">Login</a>
      </div> -->
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class=" header-inner-pages header_hide" style="position:sticky;top:0">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="{{ url('/') }}" class="scrollto">

          @if(!empty($setting->logo))
          <img src="{{ url('imgs/'.$setting->logo) }}" alt="logo">
          @else
          {{ $setting->site_title }}
          @endif
        </a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      <button class="navbar-toggler d-sm-block d-lg-none" type="button" id="nav_button">
        <span class="navbar-toggler-icon"><i class="fa fa-bars" style="font-size:25px"></i></span>
      </button>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{ url('/')}}">Home</a></li>
          <!-- <li class=""><a href="{{url('/page/about-us')}}">About Us</a></li> -->
          <li class="drop-down"><a href="#">Categories</a>
            <ul>
              @foreach( $category as $item )
              <li><a href="{{ url('category/'.$item->slug) }}">{{ $item->title }}</a></li>
              @endforeach
            </ul>
          </li>
          <!-- <li class=""><a href="{{ url('/publisher') }}">Publishers</a></li> -->
          <li class=""><a href="{{ url('/latest-reports') }}">Latest Reports</a></li>
          <!-- <li class=""><a href="{{ url('page/our-services') }}">Our Services</a></li> -->
          <!-- <li class=""><a href="{{ url('/contact') }}">Contact Us</a></li> -->
          <li class="drop-down"><a href="#">About</a>
            <ul>
              <li class=""><a href="{{url('/page/about-us')}}">About Us</a></li>
              <li class=""><a href="{{ url('page/our-services') }}">Our Services</a></li>
              <li class=""><a href="{{ url('/contact') }}">Contact Us</a></li>
            </ul>
          </li>
          <li class=" d-lg-block">
            <form method="get" action="{{ url('reports') }}">
              <div class="row mx-0">
                <div class="col-lg-9 col-9 px-0">
                  <input type="search" name="search" id="" class="form-control" placeholder="Search Report..." style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                </div>
                <div class="col-lg-3 col-3 px-lg-0 pl-0">
                  <!-- <input type="submit" class='form-control search_btn' value="Search"> -->
                  <button class='form-control search_btn' type="submit"><i class='fa fa-search'></i></button>
                </div>
              </div>
            </form>
          </li>
          <!--<li><a href="{{ url('/blog')}}">Blog</a></li>-->
          <!--<li><a href="{{ url('/ourteam')}}">Team</a></li>-->

          <!-- <li><a href="{{ url('/contact') }}">Contact</a></li>
            <li class="d-xs-block d-sm-block d-md-block d-lg-none d-xl-none"><a href="{{ url('/contact') }}">Login</a></li> -->
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
  @show


  @yield('contant')



  @section('footer')

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 footer-info">
            <!-- <h3>{{ $setting->site_title }}</h3> -->
            <img src="{{ url('imgs/'.$setting->logo) }}" alt="logo" width="50%" class="mb-1">
            <p>{{ $setting->address }}<br><br>
              <strong>Mobile :</strong><a class="" href="tel:{{ $setting->mobile }}"> {{ $setting->mobile }}</a><br>
              <strong>Email :</strong><a class="" href="mailto:{{ $setting->email }}"> {{ $setting->email }}</a><br>
            </p>
            <div class="social-links mt-3">
              @if($setting->facebook_url!='')
              <a href="{{ $setting->facebook_url }}" class="facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
              @endif
              @if($setting->twitter_url!='')
              <a href="{{ $setting->twitter_url }}" class="google-plus" target="_blank"><i class="bx bxl-twitter"></i></a>
              @endif
              @if($setting->insta_url!='')
              <a href="{{ $setting->insta_url }}" class="linkedin" target="_blank"><i class="bx bxl-linkedin"></i></a>
              @endif
              <!-- @if($setting->youtube_url!='')
              <a href="{{ $setting->youtube_url }}" class="google-plus" target="_blank"><i class="bx bxl-youtube"></i></a>
              @endif
              @if($setting->pinterest_url!='')
              <a href="{{ $setting->pinterest_url }}" class="google-plus" target="_blank"><i class="bx bxl-pinterest"></i></a>
              @endif -->
            </div>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Menu </h4>
            <ul>
              <!-- <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/') }}">Home</a></li> -->
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/page/about-us') }}">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/latest-reports') }}">Latest Report</a></li>
              <!-- <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/publisher') }}">Publisher Partnership</a></li> -->
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/page/privacy-policy') }}">Privacy policy</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('page/terms-conditions') }}">Terms & Conditions</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="{{ url('contact') }}">Contact Us</a></li>
            </ul>
          </div>



          <div class="col-lg-5 col-md-6 footer-contact">
            <h4>Subscribe Newsletter</h4>
            {{ Form::open(['action'=>'HomeController@newsletter','method'=>'POST']) }}
            <div class="row">
              <div class="col-8 pr-0">
                <input type="email" name="email" id="" class="form-control" placeholder="Enter Your Email" required style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
              </div>
              <div class='col-4 pl-0'>
                <button class='form-control text-center' style="background: red;border-top-left-radius: 0; border-bottom-left-radius: 0;color:#fff">Subscribe</button>
              </div>
              <div class="col-12 mt-4 ">
                <img src="https://affluencemarketreports.com/static/images/payment-logo.png" alt="">
              </div>
            </div>
            {{ Form::close() }}

          </div>



        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="copyright text-left">
            &copy; Copyright <strong><span>{{ $setting->site_title }}</span></strong>. All Rights Reserved
          </div>
        </div>
        <div class="col-sm-6">
          <div class="copyright text-right">
            Powered by <a href="{{ url('/') }}" class="text-white" target="_blank"><b>Affluence Market Reports</b></a>
          </div>
        </div>
      </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <!-- Vendor JS Files -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
  <script src="{{ url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ url('vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{ url('vendor/php-email-form/validate.js')}}"></script>
  <script src="{{ url('js/validation.js')}}"></script>
  <script src="{{ url('vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{ url('vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{ url('vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{ url('vendor/aos/aos.js')}}"></script>
  <script src="{{ url('js/bootstrap-pincode-input.js')}}"></script>
  <script src="{{ url('js/ajax.js')}}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#district").attr('checked', false);
      if (!$("#district").attr('checked')) {
        $(".e_dist").attr('disabled', "true");
      }
      $("#district").click(function() {
        $(".e_dist").attr('disabled', !this.checked);
      });

      $("#tehsil").attr('checked', false);
      if (!$("#tehsil").attr('checked')) {
        $(".e_teh").attr('disabled', "true");
      }
      $("#tehsil").click(function() {
        $(".e_teh").attr('disabled', !this.checked);
      });

      $("#panchayat").attr('checked', false);
      if (!$("#panchayat").attr('checked')) {
        $(".e_panchayat").attr('disabled', "true");
      }
      $("#panchayat").click(function() {
        $(".e_panchayat").attr('disabled', !this.checked);
      });

      $("#gram").attr('checked', false);
      if (!$("#gram").attr('checked')) {
        $(".e_gram").attr('disabled', "true");
      }
      $("#gram").click(function() {
        $(".e_gram").attr('disabled', !this.checked);
      });

      $("#village").attr('checked', false);
      if (!$("#village").attr('checked')) {
        $(".e_village").attr('disabled', "true");
      }
      $("#village").click(function() {
        $(".e_village").attr('disabled', !this.checked);
      });

      if (document.getElementById("testName").checked) {
        document.getElementById('testNameHidden').enable = true;
      } else {
        document.getElementById('testName').disabled = false;
      }
    });
  </script>
  <script>
    jQuery(document).ready(function($) {
      $('.hero').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: !0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 8000,
        draggable: false,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              draggable: true,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              draggable: true,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              draggable: true,
              slidesToScroll: 1
            }
          }

        ]
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        }, {
          breakpoint: 520,
          settings: {
            slidesToShow: 2
          }
        }]
      });
    });
  </script>
  <script type="text/javascript">
    $('.pincode-input2').pincodeInput({
      hidedigits: false,
      inputs: 6,
      complete: function(value, e, errorElement) {


        // $(errorElement).html("I'm sorry, but the code not correct");
      }
    });
  </script>
  <script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#lb').addClass('d-none');
          $('#profile-img-tag').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#profile-img").change(function() {
      readURL(this);
    });

    $(':radio').change(function(event) {
      var id = $(this).data('id');
      $('#' + id).addClass('none').siblings().removeClass('none');
    });
  </script>
  <script type="text/javascript">
    function printForm() {
      let content = $('body').html();

      $('body').html($('#print_area').html());

      window.print();

      $('body').html(content);
    }
  </script>
  <script>
    $('#nav_button').on('click', function() {
      $('.nav-menu').toggleClass("d-block");
      $('#nav_button').toggleClass("nav_btn_after");

    });
  </script>
  <!-- {{ Html::script('https://checkout.razorpay.com/v1/checkout.js')}} -->

  @yield('footer-script')
  {{ Html::script('js/ajax.js') }}
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <!-- {{ Html::script('https://checkout.razorpay.com/v1/checkout.js')}} -->

  @yield('footer-script')
  
</body>

</html>