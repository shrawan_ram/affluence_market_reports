<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $guarded = [
        'created_at', 
        'deleted_at', 
        'id'
    ];
    protected $hidden  = [
        'password',
        'otp_code',
        'remember_token'
    ];
}
