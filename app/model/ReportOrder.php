<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReportOrder extends Model
{
  protected $guarded = [];

  const CREATED_AT        = 'created_at';
  const UPDATED_AT        = 'updated_at';

  protected $with = ['report','countri'];

  public function report()
  {
    return $this->hasOne('App\model\Report', 'id', 'report_id');
  }
  public function countri()
  {
    return $this->hasOne('App\model\Country', 'id', 'country');
  }
}
