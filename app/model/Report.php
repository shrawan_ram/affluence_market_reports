<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

    // protected $table 		= "abouts";
    protected $guarded = [];  
    protected $with = ['cat','publisher'];

    
    public function cat(){
      return $this->hasOne('App\model\Category', 'id', 'category_id');
    }
    public function publisher(){
      return $this->hasOne('App\model\Category', 'id', 'publisher_id');
    }
    
}
