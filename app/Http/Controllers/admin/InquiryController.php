<?php

namespace App\Http\Controllers\admin;

// use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\model\Inquiry;

class InquiryController extends Controller
{

    public function index(request $request)
    {

        $query = Inquiry::latest();

        if (!empty($request->mobile_no)) {
            $query->where('mobile_no', 'LIKE', '%' . $request->mobile_no . '%');
        }


        $contact_inquiry = $query->paginate(20);
        //




        $data = compact('contact_inquiry'); // Variable to array convert
        return view('backend.inc.contact_inquiry', $data);
    }
}
