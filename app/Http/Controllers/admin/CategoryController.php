<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Category;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller{
   
    public function index(request $request){

        $query = Category::where('type','category')->latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $category = $query->get(); 
               

        $data = compact( 'category' ); // Variable to array convert
        return view('backend.inc.category.index', $data);
    }
    

    

   
    public function add()
    {
        //

        $categories = Category::where('type','category')->get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->title;
        }
        $data = compact('parentArr');
        return view('backend.inc.category.add',$data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',
            'slug'              => 'required|unique:category'

                ];
            
        $request->validate( $rules );
        
        $obj = new Category;
        $obj->title              = $request->title;
        $obj->slug               = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        // $obj->excerpt            = $request->excerpt;
        $obj->type               = $request->type;        
        $obj->parent             = $request->parent;
        $obj->seo_title          = $request->seo_title;
        $obj->seo_keywords       = $request->seo_keywords;
        $obj->seo_description    = $request->seo_description;


        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(390, 260);
            $image_resize->save(public_path('imgs/category/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/category/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Category::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $categories = Category::where('type','category')->get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->title;
        }
        


        $lists1 = Category::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit','parentArr' );

        return view('backend.inc.category.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'title' => 'required',
            'slug'           => 'required|unique:category,slug,'.$id,
            
        ];
        $request->validate( $rules );
        

        $obj = Category::findOrFail( $id );
        $obj->title              = $request->title;
        $obj->slug               = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        // $obj->excerpt            = $request->excerpt;
        $obj->type               = $request->type;        
        $obj->parent             = $request->parent;
        $obj->seo_title          = $request->seo_title;
        $obj->seo_keywords       = $request->seo_keywords;
        $obj->seo_description    = $request->seo_description;   

        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(950, 575);
            $image_resize->save(public_path('imgs/category/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/category/') )->with('success', 'Success! New record has been added.');
    }
     public function remove(  $id ){
         
        $social = Category::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Category has been deleted');
    }
     

   
}
