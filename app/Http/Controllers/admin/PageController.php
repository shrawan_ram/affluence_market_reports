<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Page;
use Illuminate\Support\Facades\Validator;



class PageController extends Controller {
    public function index(request $request) {

        $page = Page::latest()->paginate();
        

        $data = compact( 'page' ); // Variable to array convert
        return view('backend.inc.page.index', $data);
    }


    public function add(Request $request ) {

        

        return view('backend.inc.page.add');

    }

    public function addData( Request $request ) {
        $rules = [
            'title'        => 'required'
            
        ];
        $request->validate( $rules );
        

        $obj = new Page();
        $obj->title           = $request->title; 
        $obj->slug            = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->description     = $request->description;
        // $obj->excerpt         = $request->excerpt;
        $obj->seo_title       = $request->seo_title;
        $obj->seo_keywords    = $request->seo_keywords;
        $obj->seo_description = $request->seo_description;
        

        // if($request->hasFile('image'))  { 
            
        //     $image        = $request->file('image');
        //     $filename     = $image->getClientOriginalName('image');
        //     $image_resize = Image::make($image->getRealPath());              
        //     $image_resize->resize(600, 300, function($constraint)
        //     {
        //         $constraint->aspectRatio();
        //     });
        //     $image_resize->save(public_path('imgs/page/' .$filename)); 
        //     $obj->image   = $image->getClientOriginalName();     
        // }
        $obj->save();

        return redirect(url('admin-control/page'))->with('success', 'Success! A record has been updated.');
    }
   
    public function edit( Request $request, $id ) {

        $edit = Page::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.page.edit', compact('edit'));

    }
    public function editData( Request $request, $id ) {
        $rules = [
            'title'        => 'required'
            
        ];
        $request->validate( $rules );
        

        $obj = Page::findOrFail( $id );
        $obj->title           = $request->title; 
        $obj->slug            = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->description     = $request->description;
        // $obj->excerpt         = $request->excerpt;
        $obj->seo_title       = $request->seo_title;
        $obj->seo_keywords    = $request->seo_keywords;
        $obj->seo_description = $request->seo_description;
        

        // if($request->hasFile('image'))  { 
            
        //     $image        = $request->file('image');
        //     $filename     = $image->getClientOriginalName('image');
        //     $image_resize = Image::make($image->getRealPath());              
        //     $image_resize->resize(600, 300, function($constraint)
        //     {
        //         $constraint->aspectRatio();
        //     });
        //     $image_resize->save(public_path('imgs/page/' .$filename)); 
        //     $obj->image   = $image->getClientOriginalName();     
        // }
        $obj->save();

        return redirect(url('admin-control/page'))->with('success', 'Success! A record has been updated.');
    }

    public function remove(  $id ){
         
        $social = Page::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {

            $this->remove($checked);
            
        }

        return back()->with('deleted', 'Page has been deleted');
    }
    

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}
