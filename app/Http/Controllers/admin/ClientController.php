<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\Client;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller{
   
    public function index(request $request){

        $query = Client::latest();

        if( !empty( $request->name ) ) {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
        }


        $client = $query->paginate(20);        

        $data = compact( 'client' ); // Variable to array convert
        return view('backend.inc.client.index', $data);
    }
    

    

   
    public function add()
    {
        //

        
        
        return view('backend.inc.client.add');
    }

    
    
    public function addData(Request $request)
    {
        $rules = [
            'name'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
                   
        $request->validate( $rules );
        $obj = new Client;
        $obj->name             = $request->name;
        $obj->slug             = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug);
        $obj->position         = $request->position;
        $obj->message          = $request->message;       
        

        if($request->hasFile('image'))  { 
            
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/clients/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            // $banner_resize = Image::make($image->getRealPath());
            // $banner_resize->resize(750,500);
            // $banner_resize->save(public_path('imgs/blogs/original/' .$filename));
        }
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/client/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Client::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        
        


        $lists1 = Client::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit');

        return view('backend.inc.client.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        
        $rules = [
            'name'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Client::findOrFail( $id );
        $obj->name             = $request->name;
        $obj->slug             = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug);
        $obj->position         = $request->position;
        $obj->message          = $request->message;
        
        if($request->hasFile('image'))  {
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/clients/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        $obj->save();

        return redirect( url('admin-control/client') )->with('success', 'Success! A record has been updated.');
    }
    public function remove(  $id ){
         
        $social = Client::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Client has been deleted');
    }

   
}
