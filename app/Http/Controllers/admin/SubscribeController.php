<?php

namespace App\Http\Controllers\admin;

use App\model\Subscribe;

use Illuminate\Routing\Controller;

class SubscribeController extends Controller {
    public function index() {
    	
    	$subscribe	= Subscribe::latest()->paginate();
    	$data		= compact('subscribe');
        return view('backend.inc.subscribe',$data);
    }
}
