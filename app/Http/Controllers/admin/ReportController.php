<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Report;
use App\model\ReportOrder;
use App\model\ReportInquiry;
use Illuminate\Support\Facades\Validator;
use App\model\Category;
use App\model\Setting;

use App\Imports\ReportImport;
use App\Exports\ReportExport;

use App\model\BulkImport;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{

    public function index(request $request)
    {

        $query = Report::latest();

        if (!empty($request->category)) {
            $query->where('category_id',  $request->category);
        }
        if (!empty($request->from)) {
            $query->whereDate('created_at', '>=', $request->from);
        }
        if (!empty($request->to)) {
            $query->whereDate('created_at', '<=', $request->to);
        }
        
        
        $report = $query->get();
        $categories = Category::get();
        $setting = Setting::find(1);

        $data = compact('report','categories', 'setting'); // Variable to array convert
        return view('backend.inc.report.index', $data);
    }





    public function add()
    {
        $categories = Category::where('type','category')->get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach ($categories as $c) {

            $parentArr[$c->id] = $c->title;
        }
        $publishers = Category::where('type','publisher')->get();
        $publisherArr = [
            ''  => 'Select Publisher'
        ];

        foreach ($publishers as $c) {

            $publisherArr[$c->id] = $c->title;
        }
        $data = compact('parentArr','publisherArr');
        return view('backend.inc.report.add', $data);
    }


    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',
            'slug'              => 'unique:reports'
        ];
        // $file = $request->file('image','image_hover');
        
        $request->validate($rules);
        $input = $request->all();
        $input['slug'] = $input['slug'] == '' ? Str::slug($input['title']) : Str::lower($input['slug']);
        if ($request->hasFile('image')) {
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(1440, 762);
            $image_resize->save(public_path('imgs/report/' . $filename));
            $input['image']   = $image->getClientOriginalName();
        }
        $obj = new Report($input);
        $obj->save();

        $report = Report::find($obj->id);
        $report->amr_id = 'OMR-' . $report->id;
        $report->save();



        // $obj->image  = $request->$file->getClientOriginalName();


        return redirect(url('admin-control/report/'))->with('success', 'Success! New record has been added.');
    }


    public function edit(Request $request, $id)
    {
        //
        $edit = Report::findOrFail($id)->toArray();
        $edit['created_at'] = date('Y-m-d', strtotime($edit['created_at']));

        // dd($edit);

        $request->replace($edit);
        $request->flash();
        $categories = Category::where('type','category')->get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach ($categories as $c) {

            $parentArr[$c->id] = $c->title;
        }
        $publishers = Category::where('type','publisher')->get();
        $publisherArr = [
            ''  => 'Select Publisher'
        ];

        foreach ($publishers as $c) {

            $publisherArr[$c->id] = $c->title;
        }

        $lists1 = Report::latest()->paginate(20);

        $data = compact('lists1', 'edit', 'parentArr', 'publisherArr');

        return view('backend.inc.report.edit', $data);
    }


    public function editData(Request $request, $id)
    {
        $rules = [
            'title'          => 'required',

        ];
        $request->validate($rules);

        $input = $request->all();
        

        $input['slug'] = $input['slug'] == '' ? Str::slug($input['title']) : Str::lower($input['slug']);
        if ($request->hasFile('image')) {

            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(1440, 762);
            $image_resize->save(public_path('imgs/report/' . $filename));
            $input['image']   = $image->getClientOriginalName();
        }
        $obj = Report::findOrFail($id);
        $obj->fill($input);
        $obj->save();
        

        return redirect(url('admin-control/report'))->with('success', 'Success! A record has been updated.');
    }
    public function remove($id)
    {

        $social = Report::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        // dd($request);
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);


        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {
            
            $this->remove($checked);
        }

        return back()->with('deleted', 'Report has been deleted');
    }
    public function show_inquiry($type)
    {
        $reports    = ReportInquiry::where('type', $type)->latest()->paginate(10);
        // dd($reports);
        $data       = compact('reports');
        return view('backend.inc.report.inquiry', $data);
    }

    public function report_order()
    {
        $reports    = ReportOrder::with('countri')->latest()->paginate(10);
        // dd($reports);
        $data       = compact('reports');
        return view('backend.inc.report.report_order', $data);
    }

    
    public function import(Request $request) 
    {
        // $ab =   Excel::import(new BulkImport,request()->file('file'));
        // dd($ab);
        // return back();

        $csv = array();
        

        // check there are no errors
        if($_FILES['file']['error'] == 0){
            $name = $_FILES['file']['name'];
            // dd($name);
            $filearray = explode('.', $name);
            $ext = end($filearray);
            // dd(end($ext));
            $type = $_FILES['file']['type'];
            $tmpName = $_FILES['file']['tmp_name'];

            // check the file is a csv
            if($ext === 'csv'){
                if(($handle = fopen($tmpName, 'r')) !== FALSE) {
                    // necessary if a large csv file
                    set_time_limit(0);
                    
                    $row = 0;
                    $fields = $csv = [];
                    
                    while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        // number of fields in the csv
                        $col_count = count($data);

                        if($row === 0) {
                            $fields = $data;
                        } else {
                            $csv[] = array_combine($fields, $data);
                        }
                        
                        $row++;
                        
                    }
                    foreach($csv as $item) {
                        unset($item['#']);
                        // unset($item['created_at']);
                        unset($item['updated_at']);
                        unset($item['id']);
                        // dd($item);

                        // $item['created_at'] = Carbon::
                        $item['slug'] = $item['slug'] == '' ? Str::slug($item['title']) : Str::lower($item['slug']);
                        $report = new Report($item);
                        // $report->title = $item['title'];
                        $report->save();

                    }
                }
            }
        }
        return back()->with('success', 'Import Succeful your file');
    }

    public function exportCsv(Request $request) {
        $fileName = 'reports.csv';
       
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $query = Report::latest();

        if (!empty($request->category)) {
            $query->where('category_id',  $request->category);
        }
        if (!empty($request->from)) {
            $query->whereDate('created_at', '>=', $request->from);
        }
        if (!empty($request->to)) {
            $query->whereDate('created_at', '<=', $request->to);
        }
        $report = $query->get()->toArray();
        

        

        $listArr = [];
        foreach($report as $key => $task) {
            $arr = $task;
            
            // $arr = array_merge($task, $task['user']);
            unset($arr['cat']);
            $listArr[] = $arr;
        }

        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();

    }
    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
        // disable caching
            $now = gmdate("D, d M Y H:i:s");
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
            header("Last-Modified: {$now} GMT");
    
            // force download  
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
    
            // disposition / encoding on response body
            header("Content-Disposition: attachment;filename={$filename}");
            header("Content-Transfer-Encoding: binary");
        }
    
    public function importfile() 
    {
        
        Excel::import(new ReportImport,request()->file('file'));
        return back();
    }

    public function exportfile(Request $request) 
    {
        // dd($request);
        return Excel::download(new ReportExport($request), 'reports.xlsx');
    }
}
