<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\Setting;




class SettingController extends Controller {
    public function index(request $request) {

        $lists = Setting::latest()->paginate();
        

        $data = compact( 'lists' ); // Variable to array convert
        return view('backend.inc.setting.index', $data);
    }


   
    public function edit( Request $request, $id ) {

        $edit = Setting::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.setting.edit', compact('edit'));

    }
    public function editData( Request $request, $id ) {
        $rules = [
            'site_title'        => 'required',
            
        ];
        $request->validate( $rules );
        

        $obj = Setting::findOrFail( $id );
        $obj->site_title         = $request->site_title; 
        $obj->tagline            = $request->tagline;
        $obj->mobile             = $request->mobile;
        $obj->email              = $request->email;
        $obj->email_i            = $request->email_i;
        $obj->address            = $request->address;
        $obj->facebook_url       = $request->facebook_url;
        $obj->insta_url          = $request->insta_url;
        $obj->twitter_url        = $request->twitter_url;
        $obj->youtube_url        = $request->youtube_url;
        $obj->pinterest_url      = $request->pinterest_url;        
        

        if($request->hasFile('logo'))  { 
            
            $image        = $request->file('logo');
            $filename     = $image->getClientOriginalName('logo');
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/' .$filename));
            $obj->logo   = $image->getClientOriginalName();
            
        }
        if($request->hasFile('favicon'))  { 
            
            $image        = $request->file('favicon');
            $filename     = $image->getClientOriginalName('favicon');
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/' .$filename));
            $obj->favicon   = $image->getClientOriginalName();
            
        }
        if($request->hasFile('report_image'))  { 
            
            $image        = $request->file('report_image');
            $filename     = $image->getClientOriginalName('report_image');
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/' .$filename));
            $obj->report_image   = $image->getClientOriginalName();
            
        }
        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}


   
    
    
   
   

