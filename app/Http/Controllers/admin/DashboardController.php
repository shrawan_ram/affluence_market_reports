<?php

namespace App\Http\Controllers\admin;
use App\model\Category;
use App\model\Client;
use App\model\Report;
use App\model\Subscribe;
use App\model\Inquiry;
use App\model\Page;
use App\model\Setting;
use App\model\Slider;
use App\model\Faq;
use App\User;

use Illuminate\Routing\Controller;

class DashboardController extends Controller {
    public function index() {
    	$category_count	= Category::where('type','category')->count();
    	$client_count	= Client::count();
    	$subscribe_count= Subscribe::count();
    	$page_count		= Page::count();
    	$slider_count	= Slider::count();
    	$faq_count		= Faq::count();
    	$report_count	= Report::count();
    	$inquiry_count	= Inquiry::count();
		$setting		= Setting::find(1);
    	$data=compact('category_count','page_count','slider_count','faq_count','inquiry_count','report_count','subscribe_count','client_count','setting');
        return view('backend.inc.dashboard',$data);
    }
}
