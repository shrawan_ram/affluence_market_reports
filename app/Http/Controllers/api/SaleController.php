<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\model\Sale;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::latest();
        return DataTables::of($sales)
            ->addIndexColumn()
            ->editColumn('total_items', function (Sale $sale) {
                return $sale->items_count . ' Items';
            })
            ->addColumn('action', function ($sale_id) {
                $action = "
                <a href='" . route('admin.sale.show', $sale_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Print' target='_blank'><i class='icon-print'></i></a>
                <a href='" . route('admin.sale.destroy', $sale_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}