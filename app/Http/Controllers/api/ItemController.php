<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\model\Item;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Item::query();

        return DataTables::of($query)
            ->addIndexColumn()
            ->editColumn('image', function (Item $item) {
                $text = 'Not Available';
                if (!empty($item->image))
                    $text = "<img src='" . \Storage::url($item->image) . "' class='img-thumbnail table-img'>";

                return $text;
            })
            ->addColumn('action', function ($item_id) {
                $action = "
                <a href='" . route('admin.item.edit', $item_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                <a href='" . route('admin.item.destroy', $item_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }
}