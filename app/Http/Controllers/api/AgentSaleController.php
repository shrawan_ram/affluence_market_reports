<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\model\AgentSale;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgentSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = AgentSale::all();


        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($agent_sale_id) {
                $action = "
                    <a href='" . route('agent.pos.edit', $agent_sale_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                    <a href='" . route('agent.pos.destroy', $agent_sale_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
                ";
                return $action;
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\AgentSale  $agentSale
     * @return \Illuminate\Http\Response
     */
    public function show(AgentSale $agentSale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\AgentSale  $agentSale
     * @return \Illuminate\Http\Response
     */
    public function edit(AgentSale $agentSale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\AgentSale  $agentSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgentSale $agentSale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\AgentSale  $agentSale
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgentSale $agentSale)
    {
        //
    }
}