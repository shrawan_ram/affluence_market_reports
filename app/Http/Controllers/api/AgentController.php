<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\model\Agent;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Agent::withCount(['agent_sale']);


        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('agent_sale_count', function (Agent $agent) {
                $action = "No Sale";
                if ($agent->agent_sale_count) {
                    $action = "
                        <a href='" . route('admin.agent.sale', $agent->id) . "' data-toggle='tooltip' title='Edit'>Sale (" . $agent->agent_sale_count . ")</a>
                    ";
                }
                return $action;
            })
            ->addColumn('action', function ($agent_id) {
                $action = "
                <a href='" . route('admin.agent.edit', $agent_id) . "' class='btn btn-sm btn-info action-btn' data-toggle='tooltip' title='Edit'><i class='icon-pencil1'></i></a>
                <a href='" . route('admin.agent.destroy', $agent_id) . "' class='btn btn-sm btn-danger action-btn delete-btn' data-toggle='tooltip' title='Remove'><i class='icon-delete'></i></a>
            ";
                return $action;
            })
            ->rawColumns(['agent_sale_count', 'action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        //
    }
}