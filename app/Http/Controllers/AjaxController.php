<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\model\Inquiry;

class AjaxController extends BaseController {
    
    public function index(Request $request) {
        
        
        
         $data = new Inquiry();
         $data->name = $request->name;
         $data->email = $request->email;
         $data->subject = $request->subject;
         $data->message = $request->message;
         
         
         if($data->save()){
               return response()->json(['status'=>1]);
         }
         else{
                return response()->json(['status'=>0]);
         }


    }
}