<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Category;
use App\model\Setting;
use App\model\Country;
use App\model\Report;
use App\model\Faq;
use App\model\ReportInquiry;
use Mail;
use App\model\ReportOrder;
use Illuminate\Routing\Controller as BaseController;

class ReportController extends BaseController
{

    public function report(Report $slug)
    {
        // dd($slug);
        $report = $slug;

        $related = Report::where('category_id',$slug->category_id)->where('id','!=', $slug->id)->latest()->paginate(10)->toArray();
        $faq     = Faq::latest()->paginate(10);
        $countries = Country::get();
        $countryArr = [
            ''  => 'Select Country'
        ];

        foreach ($countries as $c) {
            $countryArr[$c->id] = $c->name;
        }
        $setting = Setting::find(1);
        $data = compact('report','related','countryArr','faq', 'setting');
        return view('frontend.inc.report', $data);
    }
    public function report_inquiry_form(Request $request, $type, $id)
    {
        $report_count = report::where('id',$id)->count();
        if($report_count != 0){

            $report = Report::find($id);
            
    
            $setting = Setting::findOrFail(1);
    
            $countries = Country::get();
            $countryArr = [
                ''  => 'Select Country'
            ];
    
            foreach ($countries as $c) {
                $countryArr[$c->id] = $c->name;
            }
    
            $data = compact('setting', 'countryArr', 'report', 'type');
            return view('frontend.inc.report_inquiry_form', $data);
        } else {
            return view('frontend.inc.not_found');
        }
    }
    public function report_inquiry_form_submit(Request $request)
    {

        $input = $request->except(['_token']);
        // dd($input);
        // dd($input);
        $inquery = new ReportInquiry($input);
        $setting = Setting::find(1);

        // dd($inquery);
        // $inquery->save();
        if($inquery->save()){
            $report_detail = Report::find($input['report_id'])->toArray();
            $country = Country::find($input['country'])->toArray();
            $subject = "New Report Inquiry";
            $email_params = [
                // 'to'        => env('MAIL_USERNAME'),
                'to'            => $setting->email_i,
                'reciever'      => $setting->site_title,
                'from'          => $input['email'],
                'sender'        => $input['name'],
                'report'        => $input,
                'report_detail' => $report_detail,
                'country'       => $country,
                'subject'       => $subject,
                'setting'       => $setting,
                
            ];
            // dd($email_params);
            
            // dd($email_params);
            Mail::send('email.report_inquiry', $email_params, function ($msgEmail) use ($email_params) {
                extract($email_params);
                // dd($msgEmail);

                // $msgEmail->to($to, $reciever)
                // ->subject($subject)
                // ->from($from, $sender)
                // ->replyTo($to, $reciever);
                $msgEmail->to($to, $reciever)
                    ->subject($subject)
                    ->from($from, $sender)
                    ->replyTo($to, $reciever);
            });

        }

        return redirect(url('/thank-you'))->with('success', 'inquery send successfully.');
    }

    public function searchReports(Request $request)
    {
        
        $query = Report::query();
        if ($request->search != '') {
            $query->where('title', 'like', '%'.$request->search.'%');
        }
        $reports = $query->latest()->get();
        $data = compact('reports');
        return view('frontend.inc.search_reports', $data);
    }

    public function get_checkout(Request $request, Report $slug)
    {
        $report = $slug;
        

        $countries = Country::get();
        $countryArr = [
            ''  => 'Select Country'
        ];

        foreach ($countries as $c) {
            $countryArr[$c->id] = $c->name;
        }
        $data = compact('countryArr', 'report');
        return view('frontend.inc.checkout', $data);
    }

    public function save_checkout(Request $request)
    {
        // dd($request);
        $input = $request->except(['_token']);
        // dd($input);
        $order = new ReportOrder($input);
        // dd($order);
        $order->save();

        return response()->json(['foo' => 'bar']);
    }
    public function thankyou(Request $request)
    {
        return view('frontend.inc.thankyou');
    }
    public function latest_reports()
    {
        $category   = Category::where('type', 'category')->orderBy('title')->paginate();
        $data1      = Category::paginate(10)->where('type', 'category')->toArray();
        $reports    = Report::latest()->paginate(10);
        $data       = compact('data1', 'category', 'reports');

        return view('frontend.inc.latest-reports', $data);
    }
}
