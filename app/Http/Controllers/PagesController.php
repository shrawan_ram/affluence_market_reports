<?php

namespace App\Http\Controllers;
use App\model\Page;

use Illuminate\Routing\Controller as BaseController;

class PagesController extends BaseController {
    public function index(Page $slug) {
    	  	
    	$data = compact('slug');
    	return view('frontend.inc.pages',$data);
    }
}
