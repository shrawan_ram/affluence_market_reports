<?php

namespace App\Http\Controllers;

use App\model\Client;
use App\model\Slider;
use App\model\Category;
use App\model\Faq;
use App\model\Setting;
use App\model\Report;
use App\model\Newsletter;
use App\model\Subscribe;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    public function index()
    {
        $slider      = Slider::where('is_front', 'Y')->latest()->paginate(10);
        $setting     = Setting::findOrFail(1);
        $faq         = Faq::latest()->get();
        $category    = Category::where('type', 'category')->orderBy('title', 'ASC')->get();
        $clients     = Client::latest()->get();
        $reports    = Report::latest()->paginate(15);
        // dd($slider);
        $data = compact('slider', 'faq', 'setting', 'category', 'clients', 'reports');

        return view('frontend.inc.homepage', $data);
    }

    public function newsletter(Request $request)
    {
        if (!Subscribe::where('email', $request->email)->exists()) {
            $newsletter = new Subscribe();
            $newsletter->email = $request->email;
            $newsletter->save();
        }
        return redirect()->back();
    }
}
