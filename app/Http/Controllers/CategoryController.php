<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Category;
use App\model\Product;
use App\model\Report;
use Illuminate\Routing\Controller as BaseController;

class CategoryController extends BaseController
{
    public function index(Request $request, $cslug = null)
    {
        $categoryInfo = [];
        // if (!empty($cslug)) {
        //     $categoryInfo   = Category::where('slug', $cslug)->where('category_is_deleted', 'N')->select('id')->first();
        // }

        $query = Product::with(['cat']);
        if (!empty($categoryInfo->id)) {
            $query->where('category_id', $categoryInfo->id);
        }
        if (!empty($request->s)) {
            $query->where('name', 'LIKE', '%' . $request->s . '%');
        }

        $products   = $query->paginate(1000);

        // Get All Categories
        $categories     = Category::withCount('products')->where('type', 'category')->has('products')->where('category_is_deleted', 'N')->where('parent', null)->orderBy('title', 'ASC')->get();

        $data = compact('categoryInfo', 'products', 'categories', 'cslug');
        return view('frontend.inc.service', $data);
    }
    public function single(Category $slug)
    {
        $category   = Category::where('type', 'category')->orderBy('title')->get();
        $data1      = Category::where('id', '!=', $slug->id)->where('type', 'category')->paginate(10)->toArray();
        $reports    = Report::where('category_id', $slug->id)->latest()->paginate(10);
        
        $data       = compact('slug', 'data1', 'category', 'reports');

        return view('frontend.inc.single-category', $data);
    }

    public function publisher()
    {
        $category    = Category::latest()->where('type', 'publisher')->paginate(8);
        $data       = compact('category');

        return view('frontend.inc.publisher', $data);
    }

    public function publisher_single(Category $slug)
    {
        // dd($slug->id);
        $category   = Category::latest()->where('type', 'publisher')->paginate();
        $data1      = Category::where('id', '!=', $slug->id)->where('type', 'publisher')->paginate(10)->toArray();
        $reports    = Report::where('publisher_id', $slug->id)->get();
        $data       = compact('slug', 'data1', 'category', 'reports');

        return view('frontend.inc.single-publisher', $data);
    }
}
