<?php

namespace App\Http\Controllers;
use App\model\Setting;


use Illuminate\Routing\Controller as BaseController;

class ContactController extends BaseController {
    public function index() {
    	  	
    	$setting=Setting::findOrFail(1);

    	$data=compact('setting');
    	return view('frontend.inc.contact',$data);
    }
}
