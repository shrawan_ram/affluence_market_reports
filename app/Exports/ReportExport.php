<?php

namespace App\Exports;

use App\model\Report;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportExport implements FromCollection, WithHeadings
{
    protected $request;

    function __construct($request) {
            $this->request = $request;
    }

    public function headings(): array
    {
        return ["id","title","slug","keyword","meta_title","amr_id","category_id","pages","single_user","corporate_user","report_summery","table_content","date","url"];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // dd($this->request->checked);

        $query = Report::latest();

        if (!empty($this->request->category)) {
            $query->where('category_id',  $this->request->category);
        }
        if (!empty($this->request->from)) {
            $query->whereDate('created_at', '>=', $this->request->from);
        }
        if (!empty($this->request->to)) {
            $query->whereDate('created_at', '<=', $this->request->to);
        }
        if (!empty($this->request->checked)) {
            // dd(array_values($this->request->checked));
            $checkArr = array_values($this->request->checked);
            // dd($checkArr);
                $query->whereIn('id',$checkArr);
        }
        $list = $query->select('id','title','slug','keyword','meta_title','amr_id','category_id','pages','single_user','corporate_user','report_summery','table_content', 'created_at')->get();
        
        // $list = Report::select('id','title','slug','keyword','meta_title','amr_id','category_id','pages','single_user','corporate_user','report_summery','table_content')->get();
        foreach($list as $l){
            $l->url = route('report_url',$l->id);
        }
        
        return $list;
    }
}
