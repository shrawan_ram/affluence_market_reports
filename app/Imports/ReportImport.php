<?php

namespace App\Imports;

use App\model\Report;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Str;

class ReportImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $obj = new Report([
            //
            // 'id'        => $row[0],
            'title'         => $row[1],
            'slug'          => $row[2] == '' ? Str::slug($row[1]) : Str::lower($row[2]), 
            'keyword'       => $row[3], 
            'meta_title'    => $row[4],
            'amr_id'        => $row[5],
            'category_id'   => $row[6],
            'pages'         => $row[7],
            'single_user'   => $row[8],
            'corporate_user'=> $row[9],
            'report_summery'=> $row[10],
            'table_content' => $row[11],
        ]);
        
        if(!empty($row[12])) {
            $obj->created_at = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[12]);
        }
        
        $obj->save();
        $obj->amr_id = 'OMR-'.$obj->id;
        $obj->save();
        return $obj;
    }
}
