<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplyformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applyforms', function (Blueprint $table) {
            $table->id();
            $table->string('district', 255)->nullable();
            $table->string('lsp', 255)->nullable();
            $table->enum('kiosk_owner_type', ['Individual', 'Company'])->nullable();
            $table->string('kiosk_owner_name', 255)->nullable();
            $table->string('permanent_address', 255)->nullable();
            $table->string('owner_phone_number', 255)->nullable();
            $table->string('owner_phone_mobile', 255)->nullable();
            $table->string('owner_email', 255)->nullable();
            $table->enum('kiosk_enter_location', ['urban', 'rural'])->nullable();
            $table->string('kiosk_center_name', 255)->nullable();
            $table->string('kiosk_contact', 255)->nullable();
            $table->string('kiosk_contact_person', 255)->nullable();
            $table->string('kiosk_phone_number', 255)->nullable();
            $table->string('kiosk_phone_mobile', 255)->nullable();
            $table->string('kiosk_email_id', 255)->nullable();
            $table->string('police_station', 255)->nullable();
            $table->string('kiosk_address', 255)->nullable();
            $table->string('municipal_town', 255)->nullable();
            $table->string('ward_number', 255)->nullable();
            $table->string('pincode', 255)->nullable();
            $table->string('panchayat_samiti', 255)->nullable();
            $table->string('gram_panchayat', 255)->nullable();
            $table->string('village', 255)->nullable();
            $table->string('kl_dist_detail', 255)->nullable();
            $table->enum('kl_dist_pl', ['Y', 'N'])->nullable()->default('N');
            $table->enum('kl_dist_it', ['Y', 'N'])->nullable()->default('N');
            $table->string('kl_tehsil_detail', 255)->nullable();
            $table->enum('kl_tehsil_pl', ['Y', 'N'])->nullable()->default('N');
            $table->enum('kl_tehsil_it', ['Y', 'N'])->nullable()->default('N');
            $table->string('kl_panchayat_detail', 255)->nullable();
            $table->enum('kl_panchayat_pl', ['Y', 'N'])->nullable()->default('N');
            $table->enum('kl_panchayat_it', ['Y', 'N'])->nullable()->default('N');
            $table->string('kl_gram_detail', 255)->nullable();
            $table->enum('kl_gram_pl', ['Y', 'N'])->nullable()->default('N');
            $table->enum('kl_gram_it', ['Y', 'N'])->nullable()->default('N');
            $table->string('kl_village_detail', 255)->nullable();
            $table->enum('kl_village_pl', ['Y', 'N'])->nullable()->default('N');
            $table->enum('kl_village_it', ['Y', 'N'])->nullable()->default('N');
            $table->string('account_number', 255)->nullable();
            $table->string('ifsc', 255)->nullable();
            $table->string('bank_name', 255)->nullable();
            $table->string('branch_name', 255)->nullable();
            $table->string('account_holder_name', 255)->nullable();
            $table->string('kiosk_id', 255)->nullable();
            $table->string('lsp_name', 255)->nullable();
            $table->enum('id_proof', ['Y', 'N'])->nullable()->default('N');
            $table->enum('police_verification', ['Y', 'N'])->nullable()->default('N');
            $table->enum('educational_qualification', ['Y', 'N'])->nullable()->default('N');
            $table->enum('bank_account_detail', ['Y', 'N'])->nullable()->default('N');
            $table->string('longitude', 255)->nullable();
            $table->string('latitude', 255)->nullable();
            $table->string('kiosk_owner_photo', 255)->nullable();
            $table->string('sign_kiosk_owner', 255)->nullable();
            $table->string('aadhaar_card_att', 255)->nullable();
            $table->string('pan_card', 255)->nullable();
            $table->string('10th_marksheet', 255)->nullable();
            $table->string('bank_acc_detail', 255)->nullable();
            $table->string('police_ver_att', 255)->nullable();
            $table->string('jan_aadhaar_card', 255)->nullable();
            $table->string('sso_id', 255)->nullable();
            $table->string('sign_lsp', 255)->nullable();
            $table->string('random_t', 255)->nullable();
            $table->string('_token', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applyforms');
    }
}
