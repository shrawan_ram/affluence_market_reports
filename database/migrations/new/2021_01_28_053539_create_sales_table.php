<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('agent_id');
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->string('session')->comment('financial year');
            $table->unsignedBigInteger('invoice_no')->nullable()->comment('sale invoice no Auto Generated');
            $table->string('payment_mode')->comment('like cash, card, etc.');
            $table->string('txn_number')->comment('or cheque number');
            $table->double('total_amount')->comment('to be paid');
            $table->double('paid_amount')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}